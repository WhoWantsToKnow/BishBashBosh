# BishBashBosh

This is a collection of edited extracts from the
[Bash 4.4 man page](http://man7.org/linux/man-pages/man1/bash.1.html) reformatted for
markdown (plus a little bit of html for some of the tables). In addition I've included
material from several other sources where it seemed to complement and further explain
the man page content.

I've built this collection of extracts over time as a quick means of accessing
reference information on bash. Although there are many excellent sources of help when
writing bash scripts (e.g. [1][1], [2][2], [3][3] and [4][4] for specific queries) I
very often find myself scolling through the man page to find the description of a
particular feature. This is because the man page is the definitive source of
information on bash and the tutorials, examples, etc., although excellent in
themselves, may not always focus on the specific aspects of bash that I'm interested
in.

There is no definite structure or logic to what is or isn't included. These are
simply extracts that I found myself referring to on more than one occasion and found it
convenient to save separately for future use rather than scrolling through and
searching the original source material.

## Acknowledgement

Many thanks to the authors of the bash 4.4 man page; Brian Fox (Free Software
Foundation), Chet Ramey (Case Western Reserve University) and all those who also
contributed.

Thanks also to everyone else whose work I've used but not explicitly mentioned.
Unfortunately I've not taken note of the source of the reference material I've included
but I'd generally like to include a blanket acknowledgement here.

This README brought to you courtesy of the [Acme Readme Corp.][5]

## Reporting bugs

Please use the [GitHub issue tracker][6] for any bugs or feature suggestions.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

Also see the most excellent
[first-contributions](https://github.com/Roshanjossey/first-contributions).

Contributions must be licensed as per [bash](https://www.gnu.org/software/bash/).
The contributor retains the copyright.

## License

As the bulk of the content here is derived from the bash 4.4 man page it comes within
the [license conditions and provisions of that work](https://www.gnu.org/software/bash/).

## Copyright

Bash 4.4 is Copyright (C) 1989-2016 by the Free Software Foundation, Inc.

  [1]: http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html
  [2]: https://linuxconfig.org/bash-scripting-tutorial
  [3]: http://tldp.org/LDP/abs/html/
  [4]: https://stackoverflow.com/questions/tagged/bash
  [5]: https://gist.github.com/zenorocha/4526327
  [6]: https://github.com/WhowantsToknow/gitfilters/issues
