# Conditional Expressions
## Contents
* [Primary Tests](#primary-tests)
* [Double Square Brackets "*\[\[*"](#double-square-brackets-)
* [Combining Expressions](#combining-expressions)
* [Pattern Matching](#pattern-matching)
## Primary Tests
* Conditional expressions are used by the '__[[__' compound command and the '__test__'
and '__[__' builtin commands to test file attributes and perform string and arithmetic
comparisons.
* Expressions are formed from the following unary or binary primaries.

|       Primary        | Returns True When...                                          |
|:--------------------:|:------------------------------------------------------------- |
| __*file tests*__     | _notes 1 & 2_                                                 |
| -a file              | file exists.                                                  |
| -b file              | file exists and is a block special file.                      |
| -c file              | file exists and is a character special file.                  |
| -d file              | file exists and is a directory.                               |
| -e file              | file exists.                                                  |
| -f file              | file exists and is a regular file.                            |
| -g file              | file exists and is set-group-id.                              |
| -h file              | file exists and is a symbolic link.                           |
| -k file              | file exists and its ''sticky'' bit is set.                    |
| -p file              | file exists and is a named pipe (FIFO).                       |
| -r file              | file exists and is readable.                                  |
| -s file              | file exists and has a size greater than zero.                 |
| -t fd                | file descriptor fd is open and refers to a terminal.          |
| -u file              | file exists and its set-user-id bit is set.                   |
| -w file              | file exists and is writable.                                  |
| -x file              | file exists and is executable.                                |
| -G file              | file exists and is owned by the effective group id.           |
| -L file              | file exists and is a symbolic link.                           |
| -N file              | file exists and has been modified since it was last read.     |
| -O file              | file exists and is owned by the effective user id.            |
| -S file              | file exists and is a socket.                                  |
| file1 -ef file2      | file1 and file2 refer to the same device and inode numbers.   |
| file1 -nt file2      | file1 is newer (according to modification date) than file2.   |
| file1 -nt file2      | file1 exists and file2 does not.                              |
| file1 -ot file2      | file1 is older than file2.                                    |
| file1 -ot file2      | file2 exists and file1 does not.                              |
| __*shell options*__  |  |                                                            |
| -o optname           | shell option optname is enabled.                              |
| -v varname           | shell variable varname is set (has been assigned a value).    |
| -R varname           | shell variable varname is set and is a name reference.        |
| __*strings*__        |  |                                                            |
| -z string            | length of string is zero.                                     |
| -n string            | length of string is non-zero.                                 |
| string               | length of string is non-zero.                                 |
| string1 = string2    | the strings are equal. (note 3)                               |
| string1 == string2   | the strings are equal.                                        |
| string1 != string2   | the strings are not equal.                                    |
| string1 < string2    | string1 sorts before string2 lexicographically.               |
| string1 > string2    | string1 sorts after string2 lexicographically.                |
| __*arithmetic*__     | _note 4_                                                      |
| arg1 -eq arg2        | arg1 equal to arg2                                            |
| arg1 -ne arg2        | arg1 not equal to arg2                                        |
| arg1 -lt arg2        | arg1 less than arg2                                           |
| arg1 -ge arg2        | arg1 not less than (greater than or equal) arg2               |
| arg1 -gt arg2        | arg1 greater than arg2                                        |
| arg1 -le arg2        | arg1 not greater than (less than or equal) arg2               |

##### Notes
1. Unless otherwise specified, primaries that operate on files follow symbolic links
and operate on the target of the link, rather than the link itself.
2. Bash handles several filenames specially when they are used in expressions. If the
operating system on which bash is running provides these special files, bash will use
them; otherwise it will emulate them internally with this behavior: If any file
argument to one of the primaries is of the form '__*/dev/fd/n*__', then file descriptor
'__n__' is checked. If the file argument to one of the primaries is one of
'__*/dev/stdin*__', '__*/dev/stdout*__', or '__*/dev/stderr*__', file descriptor
'__0__', '__1__', or '__2__', respectively, is checked.
3. '__=__' should be used with the test command for POSIX conformance. When used with
the '__[[__' command, this performs pattern matching as described below.
4. '__*arg1*__' and '__*arg2*__' may be positive or negative integers.

## Double Square Brackets "*[[*"
This construct will return a status of '__0__' (True) or '__1__' (False) depending on the
evaluation of the conditional expression. Expressions are composed of the primaries
described above.
* word splitting and pathname expansion are not performed on the words
between the '__*[[*__' and '__*]]*__',
* tilde expansion, parameter and variable expansion, arithmetic expansion, command
substitution, process substitution, and quote removal are performed.
* conditional operators such as '__*-f*__' must be unquoted to be recognized as
primaries.
* the '__<__' and '__>__' operators sort lexicographically using the current locale.

#### _"=="_ and _"!="_ operators
The string to the right of the operator is considered a pattern and matched according
to the rules described below under __*Pattern Matching*__, as if the __*extglob*__
shell option were enabled.

Any part of the pattern may be quoted to force the quoted portion to be matched as a
string.

The return value is '__0__' (True) if the string matches ('__==__') or does not match
('__!=__') the pattern, and '__1__' (False) otherwise.

If the __*nocasematch*__ shell option is enabled, the match is performed without regard
to the case of alphabetic characters.

The '__=__' operator is equivalent to '__==__'.

#### _"=~"_ Operator
This operator has the same precedence as '__==__' and '__!=__'. When it is used, the
string to the right of the operator is considered an extended regular expression and
matched accordingly (_as in regex(3)_). The return value is '__0__' (True) if the
string matches the pattern, and '__1__' (False) otherwise.

If the regular expression is syntactically incorrect, the conditional expression's
return value is '__2__'.

If the __*nocasematch*__ shell option is enabled, the match is performed without regard
to the case of alphabetic characters.

Any part of the pattern may be quoted to force the quoted portion to be matched as a
string.

Bracket expressions in regular expressions must be treated carefully, since normal
quoting characters lose their meanings between brackets.

If the pattern is stored in a shell variable, quoting the variable expansion forces the
entire pattern to be matched as a string.

Substrings matched by parenthesized subexpressions within the regular expression are
saved in the array variable __*BASH_REMATCH*__. The element of __*BASH_REMATCH*__ with
index '__0__' is the portion of the string matching the entire regular expression. The
element of __*BASH_REMATCH*__ with index __n__ is the portion of the string matching
the __nth__ parenthesized subexpression.

## Combining Expressions
Expressions may be combined using the following operators, listed in decreasing order
of precedence:
* __( expression )__ : Returns the value of expression. This may be used to override
the normal precedence of operators.
* __! expression__ : True if expression is false.
* __expression1 && expression2__ : True if both expression1 and expression2 are true.
* __expression1 || expression2__ : True if either expression1 or expression2 is true.
* __&&__ and __||__ operators do not evaluate __expression2__ if the value of
__expression1__ is sufficient to determine the return value of the entire conditional
expression.

## Pattern Matching
Any character that appears in a pattern, other than the special pattern characters
described below, matches itself.  The __NUL__ character may not occur in a pattern.  A
backslash escapes the following character; the escaping backslash is discarded when
matching.  The special pattern characters must be quoted if they are to be matched
literally.

The special pattern characters have the following meanings:

<!-- ********************* HTML table formatting starts here *********************** -->
<table>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            *
        </th>
        <td align="left">
            Matches any string, including the null string. When the
            <b><i>globstar</i></b> shell option is enabled, and '<b>*</b>' is used in a
            pathname expansion context, two adjacent '<b>*</b>'s used as a single
            pattern will match all files and zero or more directories and
            subdirectories. If followed by a '<b>/</b>', two adjacent '<b>*</b>'s will
            match only directories and subdirectories.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            ?
        </th>
        <td align="left">
            Matches any single character.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            [...]
        </th>
        <td align="left">
            <ul>
                <li>Matches any one of the enclosed characters.</li>
                <li>A pair of characters separated by a hyphen denotes a range
                expression; any character that falls between those two characters,
                inclusive, using the current locale's collating sequence and character
                set, is matched.</li>
                <li>If the first character following the '<b>[</b>' is a '<b>!</b>' or
                a '<b>^</b>' then any character not enclosed is matched.</li>
                <li>The sorting order of characters in range expressions is determined
                by the current locale and the values of the <b>LC_COLLATE</b> or
                <b>LC_ALL</b> shell variables, if set.</li>
                <li>To obtain the traditional interpretation of range expressions,
                where <b><i>[a-d]</i></b> is equivalent to <b><i>[abcd]</i></b>, set
                value of the <b>LC_ALL</b> shell variable to <b><i>'C'</i></b>, or
                enable the <b><i>globasciiranges</i></b> shell option.</li>
                <li>A <b>'-'</b> may be matched by including it as the first or last
                character in the set.</li>
                <li>A <b>']'</b> may be matched by including it as the first character
                in the set. </li>
            </ul>
        </td>
    </tr>
</table>
<!-- ********************* HTML table formatting stops here ********************** -->

Character classes can be specified using the syntax '__*[:class:]*__', where class is
one of the following classes defined in the POSIX standard:
<b>
alnum -+- alpha -+- ascii -+- blank -+- cntrl -+- digit -+- graph -+- lower -+- print
-+- punct -+- space -+- upper -+- word -+- xdigit
</b>

A character class matches any character belonging to that class.

The '__word__' character class matches letters, digits, and the character __'_'__.

An equivalence class can be specified using the syntax __[=c=]__, which matches all
characters with the same collation weight (as defined by the current locale) as the
character __'c'__.

The syntax __*[.symbol.]*__ matches the collating symbol __*symbol*__.

If the __*extglob*__ shell option is enabled using the __*shopt*__ builtin, several
extended pattern matching operators are recognized. In the following description, a
pattern-list is a list of one or more patterns separated by a __*'|'*__. Composite
patterns may be formed using one or more of the following sub-patterns:
  - __?(pattern-list)__ : Matches zero or one occurrence of the given patterns
  - __*(pattern-list)__ : Matches zero or more occurrences of the given patterns
  - __+(pattern-list)__ : Matches one or more occurrences of the given patterns
  - __@(pattern-list)__ : Matches one of the given patterns
  - __!(pattern-list)__ : Matches anything except one of the given patterns
