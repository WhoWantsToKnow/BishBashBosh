# Prompting

## Context

When executing interactively, bash displays the primary prompt __PS1__ when it is ready
to read a command, and the secondary prompt __PS2__ when it needs more input to
complete a command. Bash displays __PS0__ after it reads a command but before executing
it.

After the string is decoded, it is expanded via parameter expansion, command
substitution, arithmetic expansion, and quote removal, subject to the value of the
__promptvars__ shell option.

## "tput"

Many linux distributions include the __*tput*__ utility normally as part of the ncurses package (see
[tput](http://linuxcommand.org/lc3_adv_tput.php) or search for __*tput*__).

## Special Characters

Bash allows these prompt strings to be customized by inserting a number of backslash
escaped special characters described in the table below.that are decoded as follows:

<!-- ********************* HTML table formatting starts here *********************** -->
<table>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \a
        </th>
        <td align="left">
            An ASCII bell character (07).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \d
        </th>
        <td align="left">
            The date in "Weekday Month Date" format (e.g., "Tue May 26").
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \D{format}
        </th>
        <td align="left">
            The format is passed to strftime(3) and the result is inserted into the
            prompt string; an empty format results in a locale-specific time
            representation. The braces {} are required.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \e
        </th>
        <td align="left">
            An ASCII escape character (033).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \h
        </th>
        <td align="left">
            The hostname up to the first '.'.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \H
        </th>
        <td align="left">
            The hostname.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \j
        </th>
        <td align="left">
            The number of jobs currently managed by the shell.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \l
        </th>
        <td align="left">
            The basename of the shell's terminal device name.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \n
        </th>
        <td align="left">
            Newline.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \r
        </th>
        <td align="left">
            Carriage return.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \s
        </th>
        <td align="left">
            The name of the shell, the basename of $0 (the portion following the final
            slash).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \t
        </th>
        <td align="left">
            The current time in 24-hour HH:MM:SS format.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \T
        </th>
        <td align="left">
            The current time in 12-hour HH:MM:SS format.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \@
        </th>
        <td align="left">
            The current time in 12-hour am/pm format.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \A
        </th>
        <td align="left">
            The current time in 24-hour HH:MM format.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \u
        </th>
        <td align="left">
            The username of the current user.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \v
        </th>
        <td align="left">
            The version of bash (e.g., 2.00).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \V
        </th>
        <td align="left">
            The release of bash, version + patch level (e.g., 2.00.0).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \w
        </th>
        <td align="left">
           The current working directory, with $HOME abbreviated with a tilde "~" (uses
           the value of the PROMPT_DIRTRIM variable).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \W
        </th>
        <td align="left">
            The basename of the current working directory, with $HOME abbreviated with
            a tilde "~".
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \!
        </th>
        <td align="left">
            The history number of this command <i>(<b>note 1</b>)</i>.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \#
        </th>
        <td align="left">
            The command number of this command <i>(<i>note 2</i>)</i>.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \$
        </th>
        <td align="left">
            If the effective UID is 0, a #, otherwise a $.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \nnn
        </th>
        <td align="left">
            The character corresponding to the octal number nnn.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \\
        </th>
        <td align="left">
            A backslash.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \[
        </th>
        <td align="left">
            Begin a sequence of non-printing characters, which could be used to embed a
            terminal control sequence into the prompt.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            \]
        </th>
        <td align="left">
            End a sequence of non-printing characters.
        </td>
    </tr>
</table>
<!-- ********************* HTML table formatting stops here ********************** -->

__Notes:__
1) the history number of a command is its position in the history list, which may
include commands restored from the history file.
2) the command number is the position in the sequence of commands executed during the
current shell session.

## Colour
### Foreground Colour
To change the foreground prompt colour include the following sequence in the prompt
string;

    \e[X;CCmPPP...\e[m

where:-

* __\e[__ – indicates the beginning of colour definition
* __X;CCm__ – indicates the required colour
    * __X__:
        * 0 = Reset/Normal (all attributes off)
        * 1 = Bold (increased intensity)
        * 2 = Faint (decreased intensity)**
        * 3 = Italic (sometimes treated as inverse)**
        * 4 = Underline
        * 5 = Slow Blink  (< 150/minute)**
        * 6 = Rapid Blink (>= 150/minute)**
        * 7 = Reverse Video (swap foreground and background colours)
        * 8 = Conceal**
        * 9 = Crossed-out (characters legible, but marked for deletion)**
    * __CC__:
        * 30 = Black
        * 31 = Red
        * 32 = Green
        * 33 = Yellow
        * 34 = Blue
        * 35 = Purple
        * 36 = Cyan
        * 37 = White
* __PPP...__ - is the prompt text (including any escape sequences)
* __\e[m__ – indicates the end of the colour definition

** _not widely supported_

### Background Colour
To change the background prompt colour include the following sequence in the prompt
string;

    \e[CCmPPP...\e[m

where __CC__ and __PPP...__ are as described above.

### Combination Of Background And Foreground
The foreground and background colours can be set at the same time with;

    \e[X;CCm\eCCmPPP...\e[m
