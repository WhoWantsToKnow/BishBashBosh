# Job Control

## Overview
Job control refers to the ability to selectively stop (suspend) the execution of
processes and continue (resume) their execution at a later point. A user typically
employs this facility via an interactive interface supplied jointly by the operating
system kernel's terminal driver and bash.

## Implementation
The shell associates a job with each pipeline. It keeps a table of currently executing
jobs, which may be listed with the jobs command. When bash starts a job
asynchronously (in the background), it prints a line that looks like:

    [1] 25647

indicating that this job is job number __1__ and that the process ID of the last
process in the pipeline associated with this job is __25647__. All of the processes in
a single pipeline are members of the same job. Bash uses the job abstraction as the
basis for job control.

## Processes
To facilitate the implementation of the user interface to job control, the operating
system maintains the notion of a current terminal process group ID. Members of this
process group (processes whose process group ID is equal to the current terminal
process group ID) receive keyboard-generated signals such as __*SIGINT*__. These
processes are said to be in the foreground. Background processes are those whose
process group ID differs from the terminal's; such processes are immune to
keyboard-generated signals. Only foreground processes are allowed to read from or, if
the user so specifies with stty tostop, write to the terminal. Background processes
which attempt to read from (write to when stty tostop is in effect) the terminal are
sent a __*SIGTTIN*__ (__*SIGTTOU*__) signal by the kernel's terminal driver, which,
unless caught, suspends the process.

## User Interface

### Process Control
If the operating system on which bash is running supports job control, bash contains
facilities to use it. Typing the suspend character (typically __*Control+Z [^Z]*__)
while a process is running causes that process to be stopped and returns control to
bash. Typing the delayed suspend character (typically __*Control+Y*__) causes the
process to be stopped when it attempts to read input from the terminal, and control to
be returned to bash. The user may then manipulate the state of this job, using the
'__*bg*__' command to continue it in the background, the '__*fg*__' command to continue
it in the foreground, or the '__*kill*__' command to kill it. A '__*^Z*__' takes effect
immediately, and has the additional side effect of causing pending output and typeahead
to be discarded.

### Command Line Reference
There are a number of ways to refer to a job in the shell. The character '__*%*__'
introduces a job specification (__jobspec__). Job number '__n__' may be referred to as
'__*%n*__'. A job may also be referred to using a prefix of the name used to start it,
or using a substring that appears in its command line. For example, '__*%ce*__' refers
to a stopped '__*ce*__' job. If a prefix matches more than one job, bash reports an
error. Using '__*%?ce*__', on the other hand, refers to any job containing the string
'__*ce*__' in its command line. If the substring matches more than one job, bash
reports an error. The symbols '__*%%*__' and '__*%+*__' refer to the shell's notion of
the current job, which is the last job stopped while it was in the foreground or
started in the background. The previous job may be referenced using '__*%-*__'. If
there is only a single job, '__*%+*__' and '__*%-*__' can both be used to refer to that
job. In output pertaining to jobs (e.g., the output of the __*jobs*__ command), the
current job is always flagged with a '__+__', and the previous job with a '__-__'. A
single '__*\%*__' (with no accompanying job specification) also refers to the current
job.

### User Control
Simply naming a job can be used to bring it into the foreground: '__*%1*__' is a
synonym for '__*fg %1*__', bringing job '__*1*__' from the background into the
foreground. Similarly, '__*%1 &*__' resumes job __*1*__ in the background, equivalent
to '__*bg %1*__'.

## Process Management
The shell learns immediately whenever a job changes state. Normally, bash waits until
it is about to print a prompt before reporting changes in a job's status so as to not
interrupt any other output. If the '__*-b*__' option to the __*set*__ builtin command
is enabled, bash reports such changes immediately. Any trap on __*SIGCHLD*__ is
executed for each child that exits.

If an attempt to exit bash is made while jobs are stopped or (if the '__*checkjobs*__'
shell option has been enabled using the '__*shopt*__' builtin) running, the shell
prints a warning message, and, if the '__*checkjobs*__' option is enabled, lists the
jobs and their statuses. The '__*jobs*__' command may then be used to inspect their
status. If a second attempt to exit is made without an intervening command, the shell
does not print another warning, and any stopped jobs are terminated.
