# Redirection

## Overview
Before a command is executed, its input and output may be redirected using a special
notation interpreted by the shell. Redirection allows commands' file handles to be
duplicated, opened, closed, made to refer to different files, and can change the files
the command reads from and writes to. Redirection may also be used to modify file
handles in the current shell execution environment. The following redirection operators
may precede or appear anywhere within a simple command or may follow a command.
Redirections are processed in the order they appear, from left to right.

## File Descriptors (fd)
Each redirection that may be preceded by a __fd__ number may instead be preceded by a
word of the form __{varname}__. In this case, for each redirection operator except
__>&-__ and __<&-__, the shell will allocate a fd greater than or equal to 10 and
assign it to __varname__. If __>&-__ or __<&-__ is preceded by __{varname}__, the value
of __varname__ defines the __fd__ to close.

In the following descriptions, if the fd number is omitted, and the first character of
the redirection operator is __<__, the redirection refers to 'stdin' (__fd0__). If
the first character of the redirection operator is __>__, the redirection refers to
'stdout' (__fd1__).

The word following the redirection operator in the following descriptions, unless
otherwise noted, is subjected to;

1. brace expansion,
2. tilde expansion,
3. parameter and variable expansion,
4. command substitution,
5. arithmetic expansion,
6. quote removal,
7. pathname expansion,

and

8. word splitting.

If it expands to more than one word, bash reports an error.

Note that the order of redirections is significant. For example, the command;

      ls > dirlist 2>&1

directs both 'stdout' and 'stderr' to the file __dirlist__,

while the command;

      ls 2>&1 > dirlist

directs only 'stdout' to file __dirlist__, because 'stderr' was
duplicated from 'stdout' before 'stdout' was redirected to
__dirlist__.

Bash handles several filenames specially when they are used in redirections, as
described in the following table. If the operating system on which bash is running
provides these special files, bash will use them; otherwise it will emulate them
internally with the behavior described below.

|  Filename          |  Emulated Behavior                             |
|:------------------ |:---------------------------------------------- |
| /dev/fd/fd         | If fd is a valid integer, fd is duplicated.    |
| /dev/stdin         | fd 0 is duplicated.                            |
| /dev/stdout        | fd 1 is duplicated.                            |
| /dev/stderr        | fd 2 is duplicated.                            |
| /dev/tcp/host/port | see [note 1](#note-1)                          |
| /dev/udp/host/port | see [note 2](#note-2)                          |

A failure to open or create a file causes the redirection to fail.

Redirections using fd's greater than __9__ should be used with care, as they
may conflict with fd's the shell uses internally.

##### Note 1
If host is a valid hostname or Internet address, and port is an integer port number
or service name, bash attempts to open the corresponding TCP socket.

##### Note 2
If host is a valid hostname or Internet address, and port is an integer port number
or service name, bash attempts to open the corresponding UDP socket.

## Redirecting Input
Redirection of input causes the file whose name results from the expansion of __word__
to be opened for reading on __fdn__, or 'stdin' (__fd0__) if __n__ is not specified.

The general format for redirecting input is:

      [n]<word

## Output
Redirection of output causes the file whose name results from the expansion of __word__
to be opened for writing on __fdn__, or 'stdout' (__fd1__) if __n__ is not
specified. If the file does not exist it is created; if it does exist it is truncated
to zero size.

The general format for redirecting output is:

      [n]>word

If the redirection operator is __>__, and the __noclobber__ option to the __*set*__
builtin has been enabled, the redirection will fail if the file whose name results from
the expansion of __word__ exists and is a regular file. If the redirection operator is
__>|__, or the redirection operator is __>__ and the __noclobber__ option to the
__*set*__ builtin command is not enabled, the redirection is attempted even if the file
named by __word__ exists.

## Appending Output
Redirection of output in this fashion causes the file whose name results from the
expansion of __word__ to be opened for appending on __fdn__, or the
'stdout' (__fd1__) if __n__ is not specified. If the file does not
exist it is created.

The general format for appending output is:

      [n]>>word

## 'stdout' and 'stderr'
This construct allows both 'stdout' (__fd1__) and 'stderr' (__fd2__) output to be
redirected to the file whose name is the expansion of word.

There are two formats for redirecting 'stdout' and 'stderr':

      &>word
and
      >&word

Of the two forms, the first is preferred. This is semantically equivalent to

      >word 2>&1

When using the second form, word may not expand to a number or -.  If it does, other
redirection operators apply (see
[Duplicating File Descriptors (fd)](#duplicating-file-descriptors-fd))
for compatibility reasons.

## Appending 'stdout' and 'stderr'
This  construct allows both 'stdout' (fd1) and 'stderr' (fd2) output to be
appended to the file whose name is the expansion of word.

The format for appending 'stdout' and 'stderr' is:

      &>>word

This is semantically equivalent to

      >>word 2>&1

(see [Duplicating File Descriptors (fd)](#duplicating-file-descriptors-fd)).

## 'Here' Documents
This type of redirection instructs the shell to read input from the current source
until a line containing just a delimiter string (with no trailing blanks) is seen. All
of the lines read up to that point are then used as 'stdin' (or __fdn__ if __n__ is
specified) for a command.

The format of 'here' documents is:

      [n]<<[-]word
            :
            input text
            :
      delimiter

Where __word__ specifies the __delimiter__ string. No parameter and variable expansion,
command substitution, arithmetic expansion, or pathname expansion is performed on
__word__. If any part of __word__ is quoted, the __delimiter__ is the result of quote
removal on __word__, and the lines in the 'here' document are not expanded. If __word__
is unquoted, all lines of the 'here' document are subjected to parameter expansion,
command substitution, and arithmetic expansion, the character sequence __\<newline>__
is ignored, and __\__ must be used to quote the characters __\\__, __$__, and __'__.

If the redirection operator is __<<-__, then all leading tab characters are stripped
from input lines and the line containing __delimiter__. This allows 'here' documents
within shell scripts to be indented in a natural fashion.

## 'Here' Strings
A variant of 'here' documents, the format is:

      [n]<<<word

The __word__ undergoes brace expansion, tilde expansion, parameter and variable
expansion, command substitution, arithmetic expansion, and quote removal. Pathname
expansion and __word__ splitting are not performed. The result is supplied as a single
string, with a newline appended, to the command on its 'stdin' (or __fdn__ if __n__ is
specified).

## Duplicating File Descriptors (fd)
The redirection operator

      [n]<&word

is used to duplicate input __fds__. If word expands to one or more digits, the __fd__
denoted by __n__ is made to be a copy of that __fd__. If the digits in __word__ do not
specify a __fd__ open for input, a redirection error occurs. If __word__ evaluates to
__-__, __fdn__ is closed. If __n__ is not specified, 'stdin' (__fd0__) is used.

The operator

      [n]>&word

is used similarly to duplicate output __fds__. If __n__ is not specified, 'stdout'
(__fd1__) is used. If the digits in __word__ do not specify a __fd__ open for output, a
redirection error occurs. If __word__ evaluates to __-__, __fdn__ is closed. As a
special case, if __n__ is omitted, and __word__ does not expand to one or more digits
or __-__, 'stdout' and 'stderr' are redirected as described previously.

## Moving File Descriptors (fd)
The redirection operator

      [n]<&digit-

moves the __fd__ digit to __fdn__, or 'stdin' (__fd0__) if __n__ is not specified.
__Digit__ is closed after being duplicated to __n__.

Similarly, the redirection operator

      [n]>&digit-

moves the __fd__ digit to __fdn__, or 'stdout' (__fd1__) if __n__ is not specified.

## Opening File Descriptors for Input and Output
The redirection operator

      [n]<>word

causes the file whose name is the expansion of __word__ to be opened for both input and
output on __fdn__, or on __fd0__ if __n__ is not specified. If the file does not
exist, it is created.
