# Command line arguments processing (__*getopts*__)

__*getopts*__ normally parses the positional parameters ($0 - $9), but if more arguments
are given, they are parsed instead.

    getopts "optstring" name [args]

__*getopts*__ is used by shell procedures to parse positional parameters as options.

## Exit Status

Returns success if an option is found; fails if the end of options is encountered or an
error occurs.

## Shell Variables

### OPTSTRING

Contains the option letters to be recognized; if a letter is followed by a colon, the
option is expected to have an argument, which should be separated from it by white
space.

### OPTIND

Each time it is invoked, __*getopts*__ will place the next option in the shell variable
__*${name}*__, initializing __*${name}*__ if it does not exist, and the index of the
next argument to be processed into the shell variable __*${OPTIND}*__. __*${OPTIND}*__
is initialized to __1__ each time the shell or a shell script is invoked.

### OPTARG

When an option requires an argument, __*getopts*__ places that argument into the shell
variable __*${OPTARG}*__.

### OPTERR

If the shell variable __*${OPTERR}*__ has the value '0' (true), __*getopts*__ disables
the printing of error messages, even if the first character of __*${OPTSTRING}*__ is
not a colon. __*${OPTERR}*__ has the value __1__ (false) by default.

## Reporting Modes

__*getopts*__ reports errors in one of two ways. If the first character of
__*${OPTSTRING}*__ is a colon, __*getopts*__ uses a silent error reporting mode. In this
mode, no error messages are printed.

### Silent Mode

If an undefined option is encountered, __*getopts*__ places the undefined option
character found into __*${OPTARG}*__.

If a required argument is missing, __*getopts*__ places a __':'__ into __*${name}*__ and
sets __*${OPTARG}*__ to the corresponding option character.

### Active Mode

If an undefined option is encountered, __*getopts*__ places __'?'__ into __*${name}*__
and unsets __*${OPTARG}*__.

If a required argument is missing, a __'?'__ is placed in __*${name}*__,
__*${OPTARG}#*__ is unset, and a diagnostic message is printed.

## What is Required to Process Command Line Arguments?:

1. option identifier letters
1. option type (string/integer/float/range)
1. option description
1. default value
1. mandatory/optional
