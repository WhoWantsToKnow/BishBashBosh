# Variables

## Overview
1. A __*variable*__ is a __*parameter*__ denoted by a __name__.
1. A __*variable*__ has a __value__ and zero or more __*attributes*__.
1. __*Attributes*__ are assigned using the __*declare*__ builtin command.
1. The __*null string*__ is a valid __value__.
1. Once a __*variable*__ is set, it may be unset only by using the __*unset*__
builtin command.

## Variables
### Assignment
A __*variable*__ may be assigned to by a statement of the form;
```bash
            [declare | local] name[=value]
```

If __value__ is not given, the __*variable*__ is assigned the __null string__.  All
values undergo tilde expansion, parameter and variable expansion, command substitution,
arithmetic expansion, and quote removal.

If the __*variable*__ has its integer attribute set, then value is evaluated as an
arithmetic expression even if the $((...)) expansion is not used
([EXPANSION](./Expansion.md)).

Word splitting is not performed, with the exception of __"$@"__ as explained in
[PARAMETERS](./Parameters.md).

Pathname  expansion  is  not  performed.   Assignment  statements  may also appear as
arguments to declaration builtin commands;
* alias,
* declare,
* typeset,
* export,
* readonly,
* local.

### Shell Variables
There are a number of variables used by bash to make information about itself available
to processes and for processes to pass back information and instructions to bash
([SHELL VARIABLES](./Shell_Variables.md))

### Additive Assignment (+=)
In  the  context  where an assignment statement is assigning a value to a shell
variable or array index, the __*"+="*__ operator can be used to append to or add to the
variable's previous value.  This includes arguments to builtin declaration commands.

#### Integers
The value is evaluated as an arithmetic expression and added to the variable's current
value, which is also evaluated.

#### Arrays
New values are appended to the array beginning at one greater than the array's maximum
index or added as additional key-value pairs in an associative array.

#### Strings
When applied to a string-valued variable, value is expanded and appended to the
variable's value.

### Indirection
A variable can be assigned the __*nameref*__ attribute using the __-n__ option to the
__declare__ or __local__ builtin commands to create a reference to another variable.
This allows variables to be manipulated indirectly.

Whenever the nameref variable (__*&variable*__) is referenced, assigned to, unset, or
has its attributes modified, the operation is actually performed on the referenced
variable.

Indirection is commonly used within shell functions to refer to a variable whose name
is passed as an argument to the function. For instance, if a variable name is passed to
a shell function as its first argument, running;

```bash
              declare -n Pointer=${1}
```
inside a function creates a __*&variable*__ ${Pointer} whose value is the
__*variable*__ name passed as the first argument. References and assignments to
${Pointer}, and changes to its attributes, are treated as references, assignments, and
attribute modifications to the __*variable*__ whose name was passed as ${1}.

If the control __*variable*__ in a __for__ loop has the nameref attribute, the list of
words can be a list of shell variables, and a name reference will be established for
each word in the list, in turn, when the loop is executed.

Array variables cannot be given the __*nameref*__ attribute. However, nameref variables
can reference array variables and subscripted array variables. Namerefs can be unset
using the -n option to the *unset* builtin. Otherwise, if unset is executed with the
name of a __*&variable*__ as an argument, the __*variable*__ referenced will be unset.
