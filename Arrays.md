# Arrays

## Overview

* Bash provides one-dimensional _indexed_ and _associative_ array variables.
* Any variable may be used as an _indexed_ array; the __*declare*__ builtin will
explicitly declare an array.
* There is no maximum limit on the size of an array, nor any requirement that members be
_indexed_ or assigned contiguously.
* _indexed_ arrays are referenced using integers (including arithmetic expressions) and
are zero-based.
* Unless otherwise noted, _indexed_ array indices must be non-negative integers.
* _associative_ arrays are referenced using arbitrary strings.

## Declaring

An _indexed_ array is created automatically if any variable is assigned to using the
syntax;

    name[subscript]=value

The subscript is treated as an arithmetic expression that must evaluate to a number. To
explicitly declare an _indexed_ array, use;

    declare -a name

or

    declare -a name[subscript]

is also accepted; the subscript is ignored.

_associative_ arrays are created using;

    declare -A name.

Attributes may be specified for an array variable using the __*declare*__ and
__*readonly*__ builtins. Each attribute applies to all members of an array.

The __*declare*__, __*local*__, and __*readonly*__ builtins each accept a -a option to
specify an _indexed_ array and a -A option to specify an _associative_ array. If both
options are supplied, -A takes precedence. The __*read*__ builtin accepts a -a option
to assign a list of words read from the standard input to an array. The __*set*__ and
__*declare*__ builtins display array values in a way that allows them to be reused as
assignments.

## Deleting Arrays and Elements

The __*unset*__ builtin is used to destroy arrays.

    unset name[subscript]

destroys the array element at index subscript. Negative subscripts to _indexed_ arrays
are interpreted as described above. Care must be taken to avoid unwanted side effects
caused by pathname expansion.

    unset name

where name is an array, or

    unset name[subscript]

where subscript is * or @, removes the entire array.

## Initialising Arrays

Arrays are initialised using compound assignments of the form;

    name=(value-1 ... value-n)

where each value is of the form;

    [subscript]=string

This syntax is also accepted by the __*declare*__ builtin.

### Initialising Indexed Arrays

These do not require anything but a list of strings. If the optional brackets and
subscript are supplied, that index is assigned to; otherwise the index of the element
assigned is the last index assigned to by the statement plus one. Indexing starts at
zero.

### Initialising Associative Arrays

With these the subscript is required.

## Array Elements

As previously stated individual array elements may be assigned to using;

    name[subscript]=value

When assigning to an _indexed_ array, if name is subscripted by a negative number, that
number is interpreted as relative to one greater than the maximum index of name, so
negative indices count back from the end of the array, and an index of -1 references
the last element.

Any element of an array may be referenced using;

    ${name[subscript]}

The braces are required to avoid conflicts with pathname expansion.

Referencing an array variable without a subscript is equivalent to referencing the
array with a subscript of 0. Any reference to a variable using a valid subscript is
legal, and bash will create an array if necessary.

An array variable is considered set if a subscript has been assigned a value. The null
string is a valid value.

## Array Expansion

If _subscript_ is "@" or "\*", the word expands to all members of name.

### Double Quotes

    "${name[*]}"

expands to a single word with the value of each array member separated by the first
character of the IFS special variable,

    "${name[@]}"

expands each element of name to a separate word. When there are no array members,
${name[@]} expands to nothing.

If the double-quoted expansion occurs within a word, the expansion of the first
parameter is joined with the beginning part of the original word, and the expansion of
the last parameter is joined with the last part of the original word.

## Array Length

    ${#name[subscript]}

expands to the length of

    ${name[subscript]}

If subscript is "\*" or "@", the expansion is the number of elements in the array.

## Array of Keys (Indices)

It is possible to obtain the keys (indices) of an array as well as the values.

    ${!name[@]}

and

    ${!name[*]}

expand to the indices assigned in array variable name. The treatment when in double
quotes is similar to the expansion of the special parameters @ and * within double
quotes.

## Negative Subscripts

If the subscript used to reference an element of an _indexed_ array evaluates to a
number less than zero, it is interpreted as relative to one greater than the maximum
index of the array, so negative indices count back from the end of the array, and an
index of -1 references the last element.
