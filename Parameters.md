# Parameters

## Overview

1. A __*_parameter_*__ is an entity that stores _values_. It can be a _name_, a
_number_, or one of the _special characters_ listed below under [Special
Parameters](#special-parameters).
1. A __*_parameter_*__ is _set_ if it has been assigned a _value_.
1. The __*null string*__ is a valid _value_.

## Positional Parameters

A _positional parameter_ is a _parameter_ denoted by one or more digits, other than the
single digit 0. _Positional parameters_ are assigned from the shell's arguments when it
is invoked, and may be reassigned using the __*set*__ builtin command. Positional
parameters may not be assigned to with assignment statements.

When a _positional parameter_ consisting of more than a single digit is expanded, it
must be enclosed in braces.

## Special Parameters

The shell treats several parameters specially. These parameters may only be referenced;
assignment to them is not allowed.

<!-- ********************* HTML table formatting starts here *********************** -->
<table>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            *
        </th>
        <td align="left">
            Expands to the positional parameters, starting from one. When the expansion
            is not within double quotes, each positional parameter expands to a
            separate word. In contexts where it is performed, those words are subject
            to further word splitting and pathname expansion. When the expansion occurs
            within double quotes, it expands to a single word with the value of each
            parameter separated by the first character of the <b>IFS</b> special
            variable. That is, <b>"$*"</b> is equivalent to <b>"$1c$2c..."</b>, where
            <b><i>'c'</b></i> is the first character of the value of the <b>IFS</b>
            variable. If <b>IFS</b> is unset, the parameters are separated by spaces.
            If <b>IFS</b> is <i>null</i>, the parameters are joined without intervening
            separators.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            @
        </th>
        <td align="left">
            Expands to the positional parameters, starting from one. When the expansion
            occurs within double quotes, each parameter expands to a separate word.
            That is, <b>"$@"</b> is equivalent to <b>"$1" "$2" ...</b> If the
            double-quoted expansion occurs within a word, the expansion of the first
            parameter is joined with the beginning part of the original word, and the
            expansion of the last parameter is joined with the last part of the
            original word. When there are no positional parameters, <b>"$@"</b> and
            <b>$@</b> expand to nothing (i.e., they are removed).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            #
        </th>
        <td align="left">
            Expands to the number of positional parameters in decimal.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            ?
        </th>
        <td align="left">
            Expands to the exit status of the most recently executed foreground
            pipeline.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            -
        </th>
        <td align="left">
            Expands to the current option flags as specified upon invocation, by the
            <i><b>set</b></i> builtin command, or those set by the shell itself (such
            as the -i option).
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            $
        </th>
        <td align="left">
            Expands to the process ID of the shell. In a <b>()</b> subshell, it expands
            to the process ID of the current shell, not the subshell.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            !
        </th>
        <td align="left">
            Expands to the process ID of the job most recently placed into the
            background, whether executed as an asynchronous command or using the bg
            builtin.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            0
        </th>
        <td align="left">
            Expands to the name of the shell or shell script. This is set at shell
            initialization. If bash is invoked with a file of commands, <b>$0</b> is
            set to the name of that file. If bash is started with the -c option, then
            <b>$0</b> is set to the first argument after the string to be executed, if
            one is present. Otherwise, it is set to the filename used to invoke bash,
            as given by argument zero.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top">
        <th align="center">
            _
        </th>
        <td align="left">
            At shell startup, set to the absolute pathname used to invoke the shell or
            shell script being executed as passed in the environment or argument list.
            Subsequently, expands to the last argument to the previous command, after
            expansion. Also set to the full pathname used to invoke each command
            executed and placed in the environment exported to that command. When
            checking mail, this parameter holds the name of the mail file currently
            being checked.
        </td>
    </tr>
</table>
<!-- ********************* HTML table formatting stops here ********************** -->
