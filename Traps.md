# Traps

## Specifying

There will be situations when it is undesirable for a bash shell script to exit in an
uncontrolled manner, for example because input has to be provided or cleanup has to be
done. The trap statement defines and activates handlers (a list of commands) to be run
when the shell receives signals or other conditions.

The syntax for the trap statement is:

    trap [OPTIONS] [ARG] [SIGNAL LIST]

The return status of the __*trap*__ command itself is zero unless an invalid signal
specification is encountered.

If no arguments are supplied, trap prints the list of commands associated with each
signal.

where:
* OPTIONS:

        -l    print a list of signal names and their corresponding numbers
        -p    display the trap commands associated with each SIGNAL in LIST

* ARG:

        command(s) to be executed when the shell receives one of the signals in SIGNAL
        LIST.

* SIGNAL LIST

        This instructs the trap command to catch the listed signals (note 1),
        which may be signal names with or without the __SIG__ prefix, or signal
        numbers. A signal may be sent to the shell with __*"kill -signal $$"*__.

Note 1 : see [SIGNALS](./Signals.md)

If __ARG__ is absent (and __SIGNAL LIST__ is present) or `-', each specified signal is
reset to its original value.

If __ARG__ is null, each signal in __SIGNAL LIST__ is ignored by the shell and by the
commands it invokes.

If a signal is __0__ or __EXIT__, then __ARG__ is executed when the shell exits.

If a signal is __RETURN__, __ARG__ is executed each time a shell function or a script
run by the . or source builtins finishes executing.

If one of the signals is __DEBUG__, __ARG__ is executed before every simple command.

A signal may also be specified as __ERR__; in that case __ARG__ is executed each time a
simple command exits with a non-zero status except when the non-zero exit status comes
from part of an __if__ statement, or from a __while__ or __until__ loop or if a logical
__AND (&&)__ or __OR (||)__ results in a non-zero exit code, or when a command's return
status is inverted using the __!__ operator.

## Simple Example

This short script will trap Ctrl+C, upon which a message is printed. When an attempt is
made to terminate this script without specifying the KILL signal, nothing will happen:

```bash
#!/bin/bash
# traptest.sh
trap "echo Booh!" SIGINT SIGTERM
echo "pid is $$"
while : do          # This is the same as "while true".
    sleep 60        # This script is not really doing anything.
done
```

## How Bash Interprets Traps

When Bash receives a signal for which a trap has been set while waiting for a command
to complete, the trap will not be executed until the command completes. When Bash is
waiting for an asynchronous command via the wait built-in, the reception of a signal
for which a trap has been set will cause the wait built-in to return immediately with
an exit status greater than 128, immediately after which the trap is executed.

## More Examples

### Detecting When A Variable Is Used

When debugging longer scripts, you might want to give a variable the trace attribute
and trap DEBUG messages for that variable. Normally you would just declare a variable
using an assignment like VARIABLE=value. Replacing the declaration of the variable with
the following lines might provide valuable information about what your script is doing:

```bash
declare -t VARIABLE=value
trap "echo VARIABLE is being used here." DEBUG
# rest of the script
```

### Removing Rubbish Upon Exit

The whatis command relies on a database which is regularly built using the
makewhatis.cron script with cron:

```bash
#!/bin/bash
LOCKFILE=/var/lock/makewhatis.lock
# Previous makewhatis should execute successfully:
[ -f $LOCKFILE ] && exit 0
# Upon exit, remove lockfile.
trap "{ rm -f $LOCKFILE ; exit 255; }" EXIT
touch $LOCKFILE
makewhatis -u -w
exit 0
```

Also see
[these examples](https://github.com/cariadeccleston/bash-error-trapping-examples) for a
nice overview of using the trap command.
