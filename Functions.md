# Functions

## Function Definition

A shell function is an object that is called like a simple command and executes a
compound command with a new set of positional parameters. Shell functions are
declared as follows:

    [function] name [()] compound-command [redirection]

* This defines a function __*name*__.
* The reserved word __*function*__ is optional.
* If the __*function*__ reserved word is supplied, the parentheses are optional.
* The body of the function is __*compound-command*__, usually a list of commands
between __'{'__ and __'}'__, but may be any command listed under
_'[Compound Commands](https://www.gnu.org/software/bash/manual/html_node/Compound-Commands.html)'_
in the bash manual.
* If the __*function*__ reserved word is used, but the parentheses are not supplied, the
braces are required.
* __*compound-command*__ is executed whenever __*name*__ is specified as the name of a
simple command.
* When in posix mode, name may not be the name of one of the POSIX special builtins.
* Any __*redirection*__ (see [REDIRECTION](./Redirection.md)) specified when a function
is defined is performed when the function is executed.

## Execution

A shell function, defined as described above, stores a series of commands for later
execution. When the name of a shell function is used as a simple command name, the list
of commands associated with that function name is executed. The exit status of a
function definition is zero unless a syntax error occurs or a readonly function with
the same name already exists. When executed, the exit status of a function is the exit
status of the last command executed in the body. (See [Behaviour](#behaviour) below.)

Functions are executed in the context of the current shell; no new process is created
to interpret them (in contrast to the execution of a shell script). When a function
is executed, the arguments to the function become the positional parameters during its
execution. The special parameter __*#*__ is updated to reflect the change. Special
parameter __*0*__ is unchanged. The first element of the __*FUNCNAME*__ array is set
to the name of the function while the function is executing.

All other aspects of the shell execution environment are identical between a function
and its caller with these exceptions: the __*DEBUG*__ and __*RETURN*__ traps (see
[TRAPS](./Traps.md)) are not inherited unless the function has been given the trace
attribute or the __*-o functrace*__ shell option has been enabled with the __*set*__
builtin (in which case all functions inherit the __*DEBUG*__ and __*RETURN*__ traps),
and the __*ERR*__ trap is not inherited unless the __*-o errtrace*__ shell option has
been enabled.

## Behaviour

Variables local to the function may be declared with the __*local*__, __*declare*__ or
__*typeset*__ builtin command. Ordinarily, variables and their values are shared
between the function and its caller.

The __*FUNCNEST*__ variable, if set to a numeric value greater than __0__, defines a
maximum function nesting level. Function invocations that exceed the limit cause the
entire command to abort.

If the builtin command __*return*__ is executed in a function, the function completes
and execution resumes with the next command after the function call. Any command
associated with the __*RETURN*__ trap is executed before execution resumes. When a
function completes, the values of the positional parameters and the special parameter
__*#*__ are restored to the values they had prior to the function's execution.

Function names and definitions may be listed with the __*-f*__ option to the
__*declare*__ or __*typeset*__ builtin commands. The __*-F*__ option to __*declare*__
or __*typeset*__ will list the function names only (and optionally the source file and
line number, if the __*extdebug*__ shell option is enabled). Functions may be exported
so that subshells automatically have them defined with the __*-f*__ option to the
__*export*__ builtin. A function definition may be deleted using the __*-f*__ option to
the __*unset*__ builtin. Note that shell functions and variables with the same name may
result in multiple identically-named entries in the environment passed to the shell's
children. Care should be taken in cases where this may cause a problem.

Functions may be recursive. The __*FUNCNEST*__ variable may be used to limit the depth
of the function call stack and restrict the number of function invocations. By default,
no limit is imposed on the number of recursive calls.
