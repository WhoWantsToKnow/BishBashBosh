# Shell Variables

This is a selective list of variables used to interact with the shell:

1. [Environment Variables](#environment-variables)
1. ["getopts"](#getopts)
1. [Shell Option Variables](#shell-option-variables)

## Environment Variables
Some of the variables used by the shell to provide information about or by the user to
define the execution environment.

#### BASH
Expands to the full filename used to invoke this instance of bash.

#### BASHOPTS
A colon-separated list of enabled shell options. Each word in the list is a valid
argument for the -s option to the __*shopt*__ builtin command. The options appearing in
__BASHOPTS__ are those reported as on by __*shopt*__. If this variable is in the
environment when bash starts up, each shell option in the list will be enabled before
reading any startup files. This variable is read-only.

#### BASHPID
Expands to the process ID of the current bash process. This differs from $$ under
certain circumstances, such as subshells that do not require bash to be re-initialized.

#### BASH_CMDS
An associative array variable whose members correspond to the internal hash table of
commands as maintained by the __*hash*__ builtin. Elements added to this array appear
in the hash table; however, unsetting array elements currently does not cause command
names to be removed from the hash table. If __BASH_CMDS__ is unset, it loses its special
properties, even if it is subsequently reset.

#### BASH_COMMAND
The command currently being executed or about to be executed, unless the shell is
executing a command as the result of a trap, in which case it is the command executing
at the time of the trap.

#### BASH_EXECUTION_STRING
The command argument to the -c invocation option.

#### BASH_LINENO
An array variable whose members are the line numbers in source files where each
corresponding member of FUNCNAME was invoked. __*${BASH_LINENO[$i]}*__ is the line
number in the source file (__*${BASH_SOURCE[$i+1]}*__) where __*${FUNCNAME[$i]}*__ was
called (or __*${BASH_LINENO[$i-1]}*__ if referenced within another shell function). Use
LINENO to obtain the current line number.

#### BASH_REMATCH
An array variable whose members are assigned by the __*=~*__ binary operator to the
__*[[*__ conditional command. The element with index 0 is the portion of the string
matching the entire regular expression. The element with index n is the portion of the
string matching the nth parenthesized subexpression. This variable is read-only.

#### BASH_SOURCE
An array variable whose members are the source filenames where the corresponding shell
function names in the FUNCNAME array variable are defined. The shell function
__*${FUNCNAME[$i]}*__ is defined in the file __*${BASH_SOURCE[$i]}*__ and called from
__*${BASH_SOURCE[$i+1]}*__.

#### BASH_SUBSHELL
Incremented by one within each subshell or subshell environment when the shell begins
executing in that environment. The initial value is 0.

#### DIRSTACK
An array variable containing the current contents of the directory stack. Directories
An array variable containing the current contents of the directory stack. Directories
appear in the stack in the order they are displayed by the __*dirs*__ builtin.
Assigning to members of this array variable may be used to modify directories already
in the stack, but the __*pushd*__ and __*popd*__ builtins must be used to add and
remove directories. Assignment to this variable will not change the current directory.
If __DIRSTACK__ is unset, it loses its special properties, even if it is subsequently
reset.

#### EUID
Expands to the effective user ID of the current user, initialized at shell startup.
This variable is readonly.

#### FUNCNAME
An array variable containing the names of all shell functions currently in the
execution call stack. The element with index 0 is the name of any currently executing
shell function. The bottom-most element (the one with the highest index) is _"main"_.
This variable exists only when a shell function is executing. Assignments to
__FUNCNAME__ have no effect. If __FUNCNAME__ is unset, it loses its special properties,
even if it is subsequently reset.

This variable can be used with __BASH_LINENO__ and __BASH_SOURCE__. Each element of
__FUNCNAME__ has corresponding elements in __BASH_LINENO__ and __BASH_SOURCE__ to
describe the call stack. For instance, __*${FUNCNAME[$i]}*__ was called from the file
__*${BASH_SOURCE[$i+1]}*__ at line number __*${BASH_LINENO[$i]}*__. The __*caller*__
builtin displays the current call stack using this information.

#### GROUPS
An array variable containing the list of groups of which the current user is a member.
Assignments to __GROUPS__ have no effect. If __GROUPS__ is unset, it loses its special
properties, even if it is subsequently reset.

#### HISTCMD
The history number, or index in the history list, of the current command. If
__HISTCMD__ is unset, it loses its special properties, even if it is subsequently
reset.

#### HOSTNAME
Automatically set to the name of the current host.

#### HOSTTYPE
Automatically set to a string that uniquely describes the type of machine on which bash
is executing. The default is system dependent.

#### LINENO
Each time this parameter is referenced, the shell substitutes a decimal number
representing the current sequential line number (starting with 1) within a script or
function. When not in a script or function, the value substituted is not guaranteed to
be meaningful. If __LINENO__ is unset, it loses its special properties, even if it is
subsequently reset.

#### MACHTYPE
Automatically set to a string that fully describes the system type on which bash is
executing, in the standard GNU cpu-company-system format. The default is
system-dependent.

#### MAPFILE
An array variable (see Arrays below) created to hold the text read by the __*mapfile*__
builtin when no variable name is supplied.

#### OLDPWD
The previous working directory as set by the __*cd*__ command.

#### OSTYPE
Automatically set to a string that describes the operating system on which bash is
executing. The default is system-dependent.

#### PIPESTATUS
An array variable (see Arrays) containing a list of exit status values from the
processes in the most-recently-executed foreground pipeline (which may contain only a
single command).

#### PPID
The process ID of the shell's parent. This variable is readonly.

#### PWD
The current working directory as set by the __*cd*__ command.

#### RANDOM
Each time this parameter is referenced, a random integer between 0 and 32767 is
generated. The sequence of random numbers may be initialized by assigning a value to
__RANDOM__. If __RANDOM__ is unset, it loses its special properties, even if it is
subsequently reset.

#### READLINE_LINE
The contents of the readline line buffer, for use with __*bind -x*__.

#### READLINE_POINT
The position of the insertion point in the readline line buffer, for use with
__*bind-x*__.

#### REPLY
Set to the line of input read by the __*read*__ builtin command when no arguments are
supplied.

#### SECONDS
Each time this parameter is referenced, the number of seconds since shell invocation is
returned. If a value is assigned to __SECONDS__, the value returned upon subsequent
references is the number of seconds since the assignment plus the value assigned. If
__SECONDS__ is unset, it loses its special properties, even if it is subsequently
reset.

#### SHELLOPTS
A colon-separated list of enabled shell options. Each word in the list is a valid
argument for the __-o__ option to the __*set*__ builtin command. The options appearing
in __SHELLOPTS__ are those reported as on by __*set -o*__. If this variable is in the
environment when bash starts up, each shell option in the list will be enabled before
reading any startup files. This variable is read-only.

#### SHLVL
Incremented by one each time an instance of bash is started.

#### UID
Expands to the user ID of the current user, initialized at shell startup. This variable
is readonly.

## "getopts"

#### OPTARG
The value of the last option argument processed.

#### OPTIND
The index of the next argument to be processed.

#### OPTERR

If set to the value 1, bash displays error messages generated by __*getopts*__.
__OPTERR__ is initialized to 1 each time the shell is invoked or a shell script is
executed.

## Shell Option Variables
The following are some of the variables used to control how the shell operates. In some
cases, bash assigns a default value to a variable; these cases are noted below.

#### BASH_COMPAT
The value is used to set the shell's compatibility level. The value may be a decimal
number (e.g., 4.2) or an integer (e.g., 42) corresponding to the desired compatibility
level. If __BASH_COMPAT__ is unset or set to the empty string, the compatibility level
is set to the default for the current version. If __BASH_COMPAT__ is set to a value
that is not one of the valid compatibility levels, the shell prints an error message
and sets the compatibility level to the default for the current version. The valid
compatibility levels correspond to the compatibility options accepted by the
__*shopt*__ builtin. The current version is also a valid value.

#### BASH_ENV
If this parameter is set when bash is executing a shell script, its value is
interpreted as a filename containing commands to initialize the shell, as in
__*~/.bashrc*__. The value of __BASH_ENV__ is subjected to parameter expansion, command
substitution, and arithmetic expansion before being interpreted as a filename. __PATH__
is not used to search for the resultant filename.

#### BASH_XTRACEFD
If set to an integer corresponding to a valid file descriptor, bash will write the
trace output generated when __*set -x*__ is enabled to that file descriptor. The file
descriptor is closed when __BASH_XTRACEFD__ is unset or assigned a new value. Unsetting
__BASH_XTRACEFD__ or assigning it the empty string causes the trace output to be sent
to the standard error. Note that setting __BASH_XTRACEFD__ to 2 (the standard error
file descriptor) and then unsetting it will result in the standard error being closed.

#### CDPATH
The search path for the __*cd*__ command. This is a colon-separated list of directories
in which the shell looks for destination directories specified by the __*cd*__ command.
A sample value is ".:~:/usr".

#### CHILD_MAX
Set the number of exited child status values for the shell to remember. Bash will not
allow this value to be decreased below a POSIX-mandated minimum, and there is a maximum
value (currently 8192) that this may not exceed. The minimum value is system-
dependent.

#### COLUMNS
Used by the select compound command to determine the terminal width when printing
selection lists. Automatically set if the __*checkwinsize*__ option is enabled or in an
interactive shell upon receipt of a __SIGWINCH__.

#### COMPREPLY
An array variable from which bash reads the possible completions generated by a shell
function invoked by the programmable completion facility. Each array element contains
one possible completion.

#### EMACS
If bash finds this variable in the environment when the shell starts with value "t", it
assumes that the shell is running in an Emacs shell buffer and disables line editing.

#### ENV
Similar to __BASH_ENV__; used when the shell is invoked in POSIX mode.

#### EXECIGNORE
A colon-separated list of shell patterns defining the list of filenames to be ignored
by command search using __PATH__. Files whose full pathnames match one of these
patterns are not considered executable files for the purposes of completion and command
execution via __PATH__ lookup. This does not affect the behavior of the __*[*__,
__*test*__, and __*[[*__ commands. Full pathnames in the command hash table are not
subject to __EXECIGNORE__. Use this variable to ignore shared library files that have
the executable bit set, but are not executable files. The pattern matching honors the
setting of the __*extglob*__ shell option.

#### FCEDIT
The default editor for the __*fc*__ builtin command.

#### FIGNORE
A colon-separated list of suffixes to ignore when performing filename completion. A
filename whose suffix matches one of the entries in __FIGNORE__ is excluded from the
list of matched filenames. A sample value is __*".o:~"*__.

#### FUNCNEST
If set to a numeric value greater than 0, defines a maximum function nesting level.
Function invocations that exceed this nesting level will cause the current command to
abort.

#### GLOBIGNORE
A colon-separated list of patterns defining the set of filenames to be ignored by
pathname expansion. If a filename matched by a pathname expansion pattern also matches
one of the patterns in __GLOBIGNORE__, it is removed from the list of matches.

#### HISTCONTROL
A colon-separated list of values controlling how commands are saved on the history list;

* __*"ignorespace"*__: lines which begin with a space character are not saved in the
history list.
* __*"ignoredups"*__: causes lines matching the previous history entry to not be saved.
* __*"ignoreboth"*__: is shorthand for ignorespace and ignoredups.
* __*"erasedups"*__: causes all previous lines matching the current line to be removed
from the history list before that line is saved.

Any value not in the above list is ignored. If __HISTCONTROL__ is unset, or does not
include a valid value, all lines read by the shell parser are saved on the history
list, subject to the value of __HISTIGNORE__. The second and subsequent lines of a
multi-line compound command are not tested, and are added to the history regardless of
the value of __HISTCONTROL__.

#### HISTFILE
The name of the file in which command history is saved. The default value is
__*~/.bash_history*__. If unset, the command history is not saved when a shell exits.

#### HISTFILESIZE
The maximum number of lines contained in the history file. When this variable is
assigned a value, the history file is truncated, if necessary, to contain no more than
that number of lines by removing the oldest entries. The history file is also truncated
to this size after writing it when a shell exits. If the value is 0, the history file
is truncated to zero size. Non-numeric values and numeric values less than zero inhibit
truncation. The shell sets the default value to the value of __HISTSIZE__ after reading
any startup files.

#### HISTIGNORE
A colon-separated list of patterns used to decide which command lines should be saved
on the history list. Each pattern is anchored at the beginning of the line and must
match the complete line (no implicit '\*' is appended). Each pattern is tested against
the line after the checks specified by __HISTCONTROL__ are applied. In addition to the
normal shell pattern matching characters, __*'&'*__ matches the previous history line.
__*'&'*__ may be escaped using a backslash; the backslash is removed before attempting
a match. The second and subsequent lines of a multi-line compound command are not
tested, and are added to the history regardless of the value of __HISTIGNORE__. The
pattern matching honors the setting of the __*extglob*__ shell option.

#### HISTSIZE
The number of commands to remember in the command history. If the value is 0, commands
are not saved in the history list. Numeric values less than zero result in every
command being saved on the history list (there is no limit). The shell sets the default
value to 500 after reading any startup files.

#### HISTTIMEFORMAT
If this variable is set and not null, its value is used as a format string for
strftime(3) to print the time stamp associated with each history entry displayed by the
__*history*__ builtin. If this variable is set, time stamps are written to the history
file so they may be preserved across shell sessions. This uses the history comment
character to distinguish timestamps from other history lines.

#### HOME
The home directory of the current user; the default argument for the __*cd*__ builtin
command. The value of this variable is also used when performing tilde expansion.

#### HOSTFILE
Contains the name of a file in the same format as __*/etc/hosts*__ that should be read
when the shell needs to complete a hostname. The list of possible hostname completions
may be changed while the shell is running; the next time hostname completion is
attempted after the value is changed, bash adds the contents of the new file to the
existing list. If __HOSTFILE__ is set, but has no value, or does not name a readable
file, bash attempts to read __*/etc/hosts*__ to obtain the list of possible hostname
completions. When __HOSTFILE__ is unset, the hostname list is cleared.

#### IFS
The Internal Field Separator that is used for word splitting after expansion and to
split lines into words with the __*read*__ builtin command. The default value is
__*'<space><tab><newline>'*__.

#### IGNOREEOF
Controls the action of an interactive shell on receipt of an __*EOF*_ character as the
sole input. If set, the value is the number of consecutive __*EOF*__ characters which
must be typed as the first characters on an input line before bash exits. If the
variable exists but does not have a numeric value, or has no value, the default value
is 10. If it does not exist, __*EOF*__ signifies the end of input to the shell.

#### INPUTRC
The filename for the readline startup file, overriding the default of __*~/.inputrc*__.

#### LANG
Used to determine the locale category for any category not specifically selected with a
variable starting with __LC\___.

#### LC_ALL
This variable overrides the value of __LANG__ and any other __LC\___ variable
specifying a locale category.

#### LC_COLLATE
This variable determines the collation order used when sorting the results of pathname
expansion, and determines the behavior of range expressions, equivalence classes, and
collating sequences within pathname expansion and pattern matching.

#### LC_CTYPE
This variable determines the interpretation of characters and the behavior of character
classes within pathname expansion and pattern matching.

#### LC_MESSAGES
This variable determines the locale used to translate double-quoted strings preceded by
a __$__.

#### LC_NUMERIC
This variable determines the locale category used for number formatting.

#### LC_TIME
This variable determines the locale category used for data and time formatting.

#### LINES
Used by the select compound command to determine the column length for printing
selection lists. Automatically set if the checkwinsize option is enabled or in an
interactive shell upon receipt of a __SIGWINCH__.

#### MAIL
If this parameter is set to a file or directory name and the __MAILPATH__ variable is
not set, bash informs the user of the arrival of mail in the specified file or
Maildir-format directory.

#### MAILCHECK
Specifies how often (in seconds) bash checks for mail. The default is 60 seconds. When
it is time to check for mail, the shell does so before displaying the primary prompt.
If this variable is unset, or set to a value that is not a number greater than or equal
to zero, the shell disables mail checking.

#### MAILPATH
A colon-separated list of filenames to be checked for mail. The message to be printed
when mail arrives in a particular file may be specified by separating the filename from
the message with a __*'?'*__. When used in the text of the message, __*$\_*__ expands
to the name of the current mailfile e.g.:

    MAILPATH='/var/mail/bfox?"You have mail":~/shell-mail?"$_ has mail!"'

Bash can be configured to supply a default value for this variable (there is no value
by default), but the location of the user mail files that it uses is system dependent
(e.g., /var/mail/$USER).

#### PATH
The search path for commands. It is a colon-separated list of directories in which the
shell looks for commands. A zero-length (null) directory name in the value of PATH
indicates the current directory. A null directory name may appear as two adjacent
colons, or as an initial or trailing colon. The default path is system-dependent, and
is set by the administrator who installs bash. A common value is;

    /usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin

#### POSIXLY_CORRECT
If this variable is in the environment when bash starts, the shell enters posix mode
before reading the startup files, as if the __*--posix*__ invocation option had been
supplied. If it is set while the shell is running, bash enables posix mode, as if the
command __*set -o posix*__ had been executed.

#### PROMPT_COMMAND
If set, the value is executed as a command prior to issuing each primary prompt.

#### PROMPT_DIRTRIM
If set to a number greater than zero, the value is used as the number of trailing
directory components to retain when expanding the __\w__ and __\W__ prompt string
escapes. Characters removed are replaced with an ellipsis.

#### PS0
The value of this parameter is expanded and displayed by interactive shells after
reading a command and before the command is executed.

#### PS1
The value of this parameter is expanded and used as the primary prompt string. The
default value is;

    "\s-\v\$ "

#### PS2
The value of this parameter is expanded as with PS1 and used as the secondary prompt
string. The default is;

    "> "

#### PS3
The value of this parameter is used as the prompt for the __*select*__ command.

#### PS4
The value of this parameter is expanded as with __PS1__ and the value is printed before
each command bash displays during an execution trace. The first character of __PS4__ is
replicated multiple times, as necessary, to indicate multiple levels of indirection.
The default is;

    "+ "

#### SHELL
The full pathname to the shell is kept in this environment variable. If it is not set
when the shell starts, bash assigns to it the full pathname of the current user's login
shell.

#### TIMEFORMAT
The value of this parameter is used as a format string specifying how the timing
information for pipelines prefixed with the time reserved word should be displayed. The
__%__ character introduces an escape sequence that is expanded to a time value or other
information. The escape sequences and their meanings are as follows; the braces denote
optional portions.

    %% - A literal %.
    %[p][l]R - The elapsed time in seconds.
    %[p][l]U - The number of CPU seconds spent in user mode.
    %[p][l]S - The number of CPU seconds spent in system mode.
    %P - The CPU percentage, computed as (%U + %S) / %R.

where;

* __p__ - an optional digit specifying the precision, the number of fractional digits
after a decimal point. A value of __0__ causes no decimal point or fraction to be
output. At most three places after the decimal point may be specified; values of __p__
greater than __3__ are changed to __3__. If __p__ is not specified, the value __3__ is
used.

* __l__ - specifies a longer format, including minutes, of the form __*MMmSS.FFs*__.
The value of __p__ determines whether or not the fraction is included.

If this variable is not set, bash acts as if it had the value

    $'\nreal\t%3lR\nuser\t%3lU\nsys\t%3lS'

If the value is null, no timing information is displayed. A trailing newline is added
when the format string is displayed.

#### TMOUT
If set to a value greater than zero, __TMOUT__ is treated as the default timeout for
the __*read*__ builtin. The select command terminates if input does not arrive after
__TMOUT__ seconds when input is coming from a terminal. In an interactive shell, the
value is interpreted as the number of seconds to wait for a line of input after issuing
the primary prompt. Bash terminates after waiting for that number of seconds if a
complete line of input does not arrive.

#### TMPDIR
If set, bash uses its value as the name of a directory in which bash creates temporary
files for the shell's use.

#### auto_resume
This variable controls how the shell interacts with the user and job control. If this
variable is set, single word simple commands without redirections are treated as
candidates for resumption of an existing stopped job. There is no ambiguity allowed; if
there is more than one job beginning with the string typed, the job most recently
accessed is selected. The name of a stopped job, in this context, is the command line
used to start it. If set to the value exact, the string supplied must match the name of
a stopped job exactly; if set to substring, the string supplied needs to match a
substring of the name of a stopped job. The substring value provides functionality
analogous to the __"%?"__ job identifier. If set to any other value, the supplied string
must be a prefix of a stopped job's name; this provides functionality analogous to the
%string job identifier.

#### histchars
The two or three characters which control history expansion and tokenization. The
significance of these are;
* 1 - the history expansion character, the character which signals the start of
a history expansion, normally __"!"__.
* 2 - the quick substitution character, which is used as shorthand for re-running the
previous command entered, substituting one string for another in the command. The
default is __"^"__.
* 3 - (optional) indicates that the remainder of the line is a comment when found as
the first character of a word, normally __"#"__. The history comment character causes
history substitution to be skipped for the remaining words on the line. It does not
necessarily cause the shell parser to treat the rest of the line as a comment.
