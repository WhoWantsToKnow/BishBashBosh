# Expansion

## Overview

Expansion is performed on the command line after it has been split into words.

There are seven kinds of expansion and they are performed in this order;
1. [Brace Expansion](#brace-expansion),
1. [Tilde Expansion](#tilde-expansion),
1. [Parameter (and Variable) Expansion](#parameter-expansion),
1. [Command Substitution](#command-substitution),
1. [Arithmetic Expansion](#arithmetic-expansion),
1. [Word Splitting](#word-splitting),
1. [Pathname Expansion](#pathname-expansion).

After these expansions are performed, quote characters present in the original word are
removed unless they have been quoted themselves ([Quote Removal](#quote-removal)).

Only brace expansion, word splitting, and pathname expansion can change the number of
words of the expansion; other expansions expand a single word to a single word. The
only exceptions to this are the expansions of __*$@*__ and __*${name[@]}*__.

## Brace Expansion

Brace expansion is a mechanism by which arbitrary strings may be generated. This
mechanism is similar to pathname expansion, but the filenames generated need not
exist. Patterns to be brace expanded take the form of an optional preamble, followed
by either a series of comma-separated strings or a sequence expression between a pair
of braces, followed by an optional postscript. The preamble is prefixed to each
string contained within the braces, and the postscript is then appended to each
resulting string, expanding left to right.

Brace expansions may be nested. The results of each expanded string are not sorted;
left to right order is preserved.

For example,

    a{d,c,b}e

expands into

    ade ace abe

A sequence expression takes the form

    {x..y[..incr]}

where __*x*__ and __*y*__ are either integers or single characters, and incr, an
optional increment, is an integer. When integers are supplied, the expression expands
to each number between __*x*__ and __*y*__, inclusive. Supplied integers may be prefixed
with 0 to force each term to have the same width. When either __*x*__ or __*y*__ begins
with a zero, the shell attempts to force all generated terms to contain the same number
of digits, zero-padding where necessary. When characters are supplied, the expression
expands to each character lexicographically between __*x*__ and __*y*__, inclusive,
using the default C locale. Note that both __*x*__ and __*y*__ must be of the same type.
When the increment is supplied, it is used as the difference between each term. The
default increment is __1__ or __-1__ as appropriate.

Brace expansion is performed before any other expansions, and any characters special
to other expansions are preserved in the result. It is strictly textual. Bash does not
apply any syntactic interpretation to the context of the expansion or the text
between the braces.

A correctly-formed brace expansion must contain unquoted opening and closing braces,
and at least one unquoted comma or a valid sequence expression. Any incorrectly formed
brace expansion is left unchanged. A '__{__' or '__,__' may be quoted with a backslash
to prevent its being considered part of a brace expression. To avoid conflicts with
parameter expansion, the string '__${__' is not considered eligible for brace
expansion.

This construct is typically used as shorthand when the common prefix of the strings to
be generated is longer than in the above example:

    mkdir /usr/local/src/bash/{old,new,dist,bugs}

or

    chown root /usr/{ucb/{jex,edit},lib/{ex?.?*,how_ex}}

Brace expansion introduces a slight incompatibility with historical versions of _sh_.
_sh_ does not treat opening or closing braces specially when they appear as part of a
word, and preserves them in the output. bash removes braces from words as a consequence
of brace expansion.  For example, a word entered to _sh_ as __*file{1,2}*__ appears
identically in the output. The same word is output as __*file1 file2*__ after expansion
by bash. If strict compatibility with _sh_ is desired, start bash with the __+B__
option or disable brace expansion with the __+B__ option to the set command.

## Tilde Expansion

If a word begins with an unquoted tilde character ('__~__'), all of the characters
preceding the first unquoted slash (or all characters, if there is no unquoted slash)
are considered a tilde-prefix. If none of the characters in the tilde-prefix are
quoted, the characters in the tilde-prefix following the tilde are treated as a
possible login name. If this login name is the null string, the tilde is replaced with
the value of the shell parameter __*${HOME}*__. If __*${HOME}*__ is unset, the home
directory of the user executing the shell is substituted instead. Otherwise, the
tilde-prefix is replaced with the home directory associated with the specified login
name.

If the tilde-prefix is a '__~+__', the value of the shell variable __*${PWD}*__
replaces the tilde-prefix. If the tilde-prefix is a '__~-__', the value of the shell
variable __*${OLDPWD}*__, if it is set, is substituted. If the characters following the
tilde in the tilde-prefix consist of a number '__N__', optionally prefixed by a '__+__'
or a '__-__', the tilde-prefix is replaced with the corresponding element from the
directory stack, as it would be displayed by the __*dirs*__ builtin invoked with the
tilde-prefix as an argument. If the characters following the tilde in the tilde-prefix
consist of a number without a leading '__+__' or '__-__', '__+__' is assumed.

If the login name is invalid, or the tilde expansion fails, the word is unchanged.

Each variable assignment is checked for unquoted tilde-prefixes immediately following a
'__:__' or the first '__=__'. In these cases, tilde expansion is also performed.
Consequently, one may use filenames with tildes in assignments to __*${PATH}*__,
__*${MAILPATH}*__, and __*${CDPATH}*__, and the shell assigns the expanded value.

## Parameter Expansion

The '__$__' character introduces parameter expansion, command substitution, or
arithmetic expansion. The parameter name or symbol to be expanded may be enclosed in
braces '__\{...\}__', which are optional but serve to protect the variable to be expanded
from characters immediately following it which could be interpreted as part of the
name.

When braces are used, the matching ending brace is the first '__}__' not escaped by a
backslash or within a quoted string, and not within an embedded arithmetic expansion,
command substitution, or parameter expansion.

<!-- ********************* HTML table formatting starts here *********************** -->
<table>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th width="5%">
            Use Parameter Value
        </th>
        <th width="20%">
            <i>${parameter}</i>
        </th>
        <td><p>
            The braces are required when <i>'parameter'</i> is a positional parameter
            with more than one digit, or when <i>'parameter'</i> is followed by a
            character which is not to be interpreted as part of its name.
            <i>'parameter'</i> is a shell parameter as described in <b><a
            href="./Parameters.md">Parameters</a></b> or an array reference
            (see <b><a href="./Arrays.md">Arrays</a></b>).
            </p><p>
            In each of the cases discussed, <i>'parameter'</i> is subject to
            <ul>
            <li><a href="#tilde-expansion">Tilde</a>,</li>
            <li><a href="#parameter-expansion">Parameter</a>,</li>
            <li><a href="#command-expansion">Command</a>,</li>
            <li><a href="#arithmetic-expansion">Arithmetic</a>,</li>
            </ul>
            </p><p align="center">
            expansion.
            </p><p>
            <br/>When not performing substring expansion, using the forms documented
            below (e.g., '<b>:-</b>'), bash tests for a <i>'parameter'</i> that is
            unset or null. Omitting the colon results in a test only for a
            <i>'parameter'</i> that is unset.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Indirect Expansion
        </th>
        <th>
            <i>${!parameter}</i>
        </th>
        <td><h4>not a <i>'nameref'</h4>
            If the first character of <i>'parameter'</i> is an exclamation point
            '<b>!</b>' it introduces a level of variable indirection. Bash uses the
            value of the variable formed from the rest of <i>'parameter'</i> as the
            name of the variable; this variable is then expanded and that value is used
            in the rest of the substitution, rather than the value of
            <i>'parameter'</i> itself. This is known as <i>'indirect expansion'</i>.
            <h4>is a <i>'nameref'</h4>
            Expands to the name of the variable referenced by <i>'parameter'</i>
            instead of performing the complete indirect expansion. The exceptions to
            this are the expansions of <b>${!prefix*}</b> and <b>${!name[@]}</b>
            described below. The exclamation point must immediately follow the left
            brace in order to introduce indirection.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Use Default Values
        </th>
        <th>
            <i>${parameter:-word}</i>
        </th>
        <td>
            If <i>'parameter'</i> is unset or null, the expansion of <i>'word'</i> is
            substituted. Otherwise, the value of <i>'parameter'</i> is substituted.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Assign Default Values
        </th>
        <th>
            <i>${parameter:=word}</i>
        </th>
        <td>
            If <i>'parameter'</i> is unset or null, the expansion of <i>'word'</i> is
            assigned to <i>'parameter'</i>. The value of <i>'parameter'</i> is then
            substituted. Positional parameters and special parameters may not be
            assigned to in this way.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Display Error if Null or Unset
        </th>
        <th>
            <i>${parameter:?word}</i>
        </th>
        <td>
            If <i>'parameter'</i> is null or unset, the expansion of <i>'word'</i> (or
            a message to that effect if <i>'word'</i> is not present) is written to the
            standard error and the shell, if it is not interactive, exits. Otherwise,
            the value of <i>'parameter'</i> is substituted.
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Use Alternate Value
        </th>
        <th>
            <i>${parameter:+word}</i>
        </th>
        <td>
            If <i>'parameter'</i> is null or unset, nothing is substituted, otherwise
            the expansion of <i>'word'</i> is substituted.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Substring Expansion
        </th>
        <th>
            <i>${parameter:offset}</i>
            <i>${parameter:offset:length}</i>
        </th>
        <td>
            <p>
            Expands to up to <i>'length'</i> characters of the value of
            <i>'parameter'</i> starting at the character specified  by
            <i>'offset'</i>. If <i>'parameter'</i> is <b>@</b>, an indexed array
            subscripted by <b>@</b> or <b>*</b>, or an associative array name, the
            results differ as described below. If <i>'length'</i> is omitted, expands
            to the substring of the value of <i>'parameter'</i> starting at the
            character specified by <i>'offset'</i> and extending to the end of the
            value.  <i>'length'</i> and <i>'offset'</i> are arithmetic expressions
            (see <a href="#arithmetic-evaluation">Arithmetic Evaluation</a>).
            </p><p>
            If <i>'offset'</i> evaluates to a number less than zero, the value is used
            as an offset in characters from the end of the value of <i>'parameter'</i>.
            If <i>'length'</i> evaluates to a number less than zero, it is interpreted
            as an offset in characters from the end of the value of <i>'parameter'</i>
            rather than a number of characters, and the expansion is the characters
            between <i>'offset'</i> and that result. Note that a negative offset must
            be separated from the colon by at least one space to avoid being confused
            with the <i>':-'</i> expansion.
            </p><p>
            If <i>'parameter'</i> is <i>'@'</i>, the result is <i>'length'</i>
            positional parameters beginning at <i>'offset'</i>. A negative offset is
            taken relative to one greater than the greatest positional parameter, so an
            <i>'offset'</i> of <b>-1</b> evaluates to the last positional parameter. It
            is an expansion error if <i>'length'</i> evaluates to a number less than
            zero.
            </p><p>
            If <i>'parameter'</i> is an indexed array name subscripted by <b>@</b> or
            <b>*</b>, the result is <i>'length'</i> members of the array beginning
            with <b><i>${parameter[offset]}</i></b>. A negative <i>'offset'</i> is
            taken relative to one greater than the maximum index of the specified
            array. It is an expansion error if <i>'length'</i> evaluates to a number
            less than zero.
            </p><p>
            Substring expansion applied to an associative array produces undefined
            results.
            </p><p>
            Substring indexing is zero-based unless the positional parameters are used,
            in which case the indexing starts at <b>1</b> by default. If
            <i>'offset'</i> is <b>0</b>, and the positional parameters are used,
            <b>$0</b> is prefixed to the list.
            </p>
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Names Matching Prefix
        </th>
        <th>
            <i>${!prefix*}</i><br/>
            <i>${!prefix@}</i>
        </th>
        <td>
            Expands to the names of variables whose names begin with <i>'prefix'</i>,
            separated by the first character of the <i><b>IFS</b></i> special variable.
            When <b>'@'</b> is used and the expansion appears within double quotes,
            each variable name expands to a separate word.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            List Of Array Keys
        </th>
        <th>
            <i>${!name[@]}</i><br/>
            <i>${!name[*]}</i>
        </th>
        <td>
            If <i>'name'</i> is an array variable, expands to the list of array indices
            (keys) assigned in <i>'name'</i>. If <i>'name'</i> is not an array, expands
            to <b>0</b> if <i>'name'</i> is set and null otherwise. When <b>@</b> is
            used and the expansion appears within double quotes, each key expands to a
            separate word.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Parameter Length
        </th>
        <th>
            <i>${#parameter}</i>
        </th>
        <td>
            The length in characters of the value of <i>'parameter'</i> is substituted.
            If <i>'parameter'</i> is <b>*</b> or <b>@</b>, the value substituted is the
            number of positional parameters. If <i>'parameter'</i> is an array name
            subscripted by <b>*</b> or <b>@</b>, the value substituted is the number of
            elements in the array.  If <i>'parameter'</i> is an indexed array name
            subscripted by a negative number, that number is interpreted as relative to
            one greater than the maximum index of <i>'parameter'</i>, so negative
            indices count back from the end of the array, and an index of <b>-1</b>
            references the last element.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Remove Matching Prefix Pattern
        </th>
        <th>
            <i>${parameter#word}</i><br/>
            <i>${parameter##word}</i>
        </th>
        <td>
            The word is expanded to produce a pattern just as in pathname expansion. If
            the pattern matches the beginning of the value of parameter, then the
            result of the expansion is the expanded value of parameter with the
            shortest matching pattern (the '<b>#</b>' case) or the longest matching
            pattern (the '<b>##</b>' case) deleted. If parameter is '<b>@</b>' or
            '<b>*</b>', the pattern removal operation is applied to each positional
            parameter in turn, and the expansion is the resultant list. If parameter is
            an array variable subscripted with '<b>@</b>' or '<b>*</b>', the pattern
            removal operation is applied to each member of the array in turn, and the
            expansion is the resultant list.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Remove Matching Suffix Pattern
        </th>
        <th>
            <i>${parameter%word}</i><br/>
            <i>${parameter%%word}</i>
        </th>
        <td>
            The word is expanded to produce a pattern just as in pathname expansion. If
            the pattern matches a trailing portion of the expanded value of parameter,
            then the result of the expansion is the expanded value of parameter with
            the shortest matching pattern (the '<b>%</b>' case) or the longest matching
            pattern (the '<b>%%</b>' case) deleted. If parameter is '<b>@</b>' or
            '<b>*</b>', the pattern removal operation is applied to each positional
            parameter in turn, and the expansion is the resultant list. If parameter is
            an array variable subscripted with '<b>@</b>' or '<b>*</b>', the pattern
            removal operation is applied to each member of the array in turn, and the
            expansion is the resultant list.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Pattern Substitution
        </th>
        <th>
            <i>${parameter/pattern/string}</i>
        </th>
        <td>
            The pattern is expanded to produce a pattern just as in pathname expansion.
            Parameter is expanded and the longest match of pattern against its value is
            replaced with string. If pattern begins with '<b>/</b>', all matches of
            pattern are replaced with string. Normally only the first match is
            replaced. If pattern begins with '<b>#</b>', it must match at the beginning
            of the expanded value of parameter. If pattern begins with '<b>%</b>', it
            must match at the end of the expanded value of parameter. If string is
            null, matches of pattern are deleted and the '<b>/</b>' following pattern
            may be omitted. If the nocasematch shell option is enabled, the match is
            performed without regard to the case of alphabetic characters. If parameter
            is '<b>@</b>' or '<b>*</b>', the substitution operation is applied to each
            positional parameter in turn, and the expansion is the resultant list. If
            parameter is an array variable subscripted with '<b>@</b>' or '<b>*</b>',
            the substitution operation is applied to each member of the array in turn,
            and the expansion is the resultant list.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Case  Modification
        </th>
        <th>
            <i>${parameter^pattern}</i><br/>
            <i>${parameter^^pattern}</i><br/>
            <i>${parameter,pattern}</i><br/>
            <i>${parameter,,pattern}</i>
        </th>
        <td>
            This expansion modifies the case of alphabetic characters in parameter. The
            pattern is expanded to produce a pattern just as in pathname expansion.
            Each character in the expanded value of parameter is tested against
            pattern, and, if it matches the pattern, its case is converted. The pattern
            should not attempt to match more than one character. The '<b>^</b>'
            operator converts lowercase letters matching pattern to uppercase; the
            '<b>,</b>' operator converts matching uppercase letters to lowercase. The
            '<b>^^</b>' and '<b>,,</b>' expansions convert each matched character in
            the expanded value; the '<b>^</b>' and '<b>,</b>' expansions match and
            convert only the first character in the expanded value. If pattern is
            omitted, it is treated like a '<b>?</b>', which matches every character. If
            parameter is '<b>@</b>' or '<b>*</b>', the case modification operation is
            applied to each positional parameter in turn, and the expansion is the
            resultant list. If parameter is an array variable subscripted with
            '<b>@</b>' or '<b>*</b>', the case modification operation is applied to
            each member of the array in turn, and the expansion is the resultant list.
        </td>
    </tr>
    <!-- row -------------------------------------------------------------------------->
    <tr valign="top" align="left">
        <th>
            Parameter  Transformation
        </th>
        <th>
            <i>${parameter@operator}</i>
        </th>
        <td>
            The  expansion  is either a transformation of the value of parameter or
            information about parameter itself, depending on the value of operator.
            Each operator is a single letter:
            <!-- sub-table starts here vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv -->
            <table>
                <!-- row -------------------------------------------------------------->
                <tr valign="top" align="left">
                    <th>
                        Q
                    </th>
                    <td>
                        The expansion is a string that is the value of parameter quoted
                        in a format that can be reused as input.
                    </td>
                </tr>
                <!-- row -------------------------------------------------------------->
                <tr valign="top" align="left">
                    <th>
                        E
                    </th>
                    <td>
                        The expansion is a string that is the value of parameter with
                        backslash escape sequences expanded as with the $'...'
                        quoting mechansim.
                    </td>
                </tr>
                <!-- row -------------------------------------------------------------->
                <tr valign="top" align="left">
                    <th>
                        P
                    </th>
                    <td>
                        The expansion is a string that is the result of expanding
                        the value of parameter as if it were a prompt string
                        (<b><a href="./Prompting.md">PROMPTING</a></b>).
                    </td>
                </tr>
                <!-- row -------------------------------------------------------------->
                <tr valign="top" align="left">
                    <th>
                        A
                    </th>
                    <td>
                        The expansion is a string in the form of an assignment
                        statement or declare command that, if evaluated, will
                        recreate parameter with its attributes and value.
                    </td>
                </tr>
                <!-- row -------------------------------------------------------------->
                <tr valign="top" align="left">
                    <th>
                        a
                    </th>
                    <td>
                        The expansion is a string consisting of flag values
                        representing parameter's attributes.
                    </td>
                </tr>
            </table>
            <!-- sub-table ends here ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->
            <p>
            If  parameter is '<b>@</b>' or '<b>*</b>', the operation is applied to each
            positional parameter in turn, and the expansion is the resultant list. If
            parameter is an array variable subscripted with '<b>@</b>' or '<b>*</b>',
            the case modification operation is applied  to  each  member  of  the array
            in turn, and the expansion is the resultant list.
            </p><p>
            The result of the expansion is subject to word splitting and pathname
            expansion as described below.
            </p>
        </td>
    </tr>
</table>
<!-- ********************* HTML table formatting stops here ********************** -->

## Command Substitution

Command substitution allows the output of a command to replace the command name.  There
are two forms:

    $(command)
or (deprecated)

    'command'

Bash performs the expansion by executing command in a subshell environment and
replacing the command substitution with the standard output of the command, with any
trailing newlines deleted. Embedded newlines are not deleted, but they may be removed
during word splitting. The command substitution __*$(cat file)*__ can be replaced by
the equivalent but faster __*$(< file)*__.

When the old-style backquote form of substitution is used, backslash retains its
literal meaning except when followed by __$__, __'__, or __\\__. The first backquote not
preceded by a backslash terminates the command substitution. When using the
__*$(command)*__ form, all characters between the parentheses make up the command; none
are treated specially.

Command substitutions may be nested. To nest when using the backquoted form, escape
the inner backquotes with backslashes.

If the substitution appears within '__"__', [word splitting](#word-splitting) and
[pathname expansion](#pathname-expansion) are not performed on the results.

## Arithmetic Expansion

Arithmetic expansion allows the evaluation of an arithmetic expression and the
substitution of the result. The format for arithmetic expansion is:

      $((expression))

The expression is treated as if it were within double quotes, but a double quote inside
the parentheses is not treated specially. All tokens in the expression undergo
parameter and variable expansion, command substitution, and quote removal. The result
is treated as the arithmetic expression to be evaluated. Arithmetic expansions may be
nested.

The evaluation is performed according to the rules listed below. If expression is
invalid, bash prints a message indicating failure and no substitution occurs.

### Arithmetic Evaluation

The shell allows arithmetic expressions to be evaluated, under certain circumstances
(see the __*let*__ and __*declare*__ builtin commands, the __*((*__ compound command,
and, obviously, [Arithmetic Expansion](#arithmetic-expansion)). Evaluation is done in
fixed-width integers with no check for overflow, though division by 0 is trapped and
flagged as an error. The operators and their precedence, associativity, and values are
the same as in the __*C language*__. Operators are evaluated in order of precedence.
Sub-expressions in parentheses are evaluated first and may override the precedence
rules.

#### Operator List

 | Operator       | Operation                                | Use            | Meaning                                  |
 | --------------:|:---------------------------------------- |:--------------:|:----------------------------------------:|
 | id++           | post-increment (add1)                    | a++            | a; a=a+1                                 |
 | ++id           | pre-increment (add1)                     | ++a            | a=a+1; a                                 |
 | id--           | post-decrement (subtract1)               | a--            | a; a=a-1                                 |
 | --id           | pre-decrement (subtract1)                | --a            | a=a-1; a                                 |
 | +              | unary plus                               | +a             | +(a)                                     |
 | -              | unary minus                              | -a             | -(a)                                     |
 | !              | logical negation                         | !a             | NOT a [where 0 is True else False]       |
 | ~              | bitwise negation                         | ~a             | e.g. 10101010 => 01010101                |
 | **             | exponentiation                           | a**b           | a=3; b=4; c=a**b [c=81]                  |
 | *              | multiplication                           | a*b            | a=3; b=4; c=a*b [c=12]                   |
 | /              | division                                 | a/b            | a=17; b=5; c=a/b [c=3]                   |
 | %              | remainder                                | a%b            | a=17; b=5; c=a%b [c=2]                   |
 | +              | addition                                 | a+b            | a=3; b=4; c=a+b [c=7]                    |
 | -              | subtraction                              | a-b            | a=3; b=4; c=a-b [c=-1]                   |
 | <<             | left bitwise shift                       | a<<b           | a=16; b=3; c=a<<b [c=128]                |
 | >>             | right bitwise shift                      | a>>b           | a=16; b=3; c=a>>b [c=2]                  |
 | <              | less than                                | a<b            | a=3; b=4; a<b [True]                     |
 | >              | greater than                             | a>b            | a=3; b=4; a>b [False]                    |
 | <=             | less than or equal to (not greater than) | a<=b           | a=3; b=3; a<=b [True]                    |
 | >=             | greater than or equal to (not less than) | a>=b           | a=4; b=4; a>=b [True]                    |
 | ==             | equality                                 | a==b           | a=4; b=4; a==b [True]                    |
 | !=             | inequality                               | a!=b           | a=4; b=4; a!=b [False]                   |
 | &              | bitwise AND                              | a&b            | a=7 [111]; b=5 [101]; c=a&b (c=5 [101])  |
 | ^              | bitwise exclusive OR                     | a^b            | a=7 [111]; b=5 [101]; c=a^b (c=2 [010])  |
 | \|             | bitwise OR                               | a\|b           | a=7 [111]; b=5 [101]; c=a\|b (c=7 [111]) |
 | &&             | logical AND                              | a==b && a==c   | a=3; b=4; c=3; a==b && a==c [False]      |
 | \|\|           | logical OR                               | a==b \|\| a==c | a=3; b=4; c=3; a==b \|\| a==c [True]     |
 | expr?expr:expr | ternary**operator                        | a=b?6:7        | a=3; b=4; c=(a=b)?6:7 [c=7]              |
 | =              | simple assignment                        | a=b            | a=b                                      |
 | *=             | multiplication                           | a*=b           | a=(a*b)                                  |
 | /=             | division                                 | a/=b           | a=(a/b)                                  |
 | %=             | remainder                                | a%=b           | a=(a%b)                                  |
 | +=             | addition                                 | a+=b           | a=(a+b)                                  |
 | -=             | subtraction                              | a-=b           | a=(a-b)                                  |
 | <<=            | bit-shift left                           | a<<=b          | a=(a<<b)                                 |
 | >>=            | bit-shift right                          | a>>=b          | a=(a>>b)                                 |
 | &=             | bitwise "and"                            | a&=b           | a=(a&b)                                  |
 | ^=             | bitwise "exclusive or"                   | a^=b           | a=(a^b)                                  |
 | \|=            | bitwise "or"                             | a\|=b          | a=(a\|b)                                 |
 | expr1,expr2    | chain expression evaluation              | (a=3, b=4)     | c=(a=3, b=4) [a=3, b=4, c=4]             |


__**__
*The ternary operator is an operator that takes three arguments. The first argument
is a comparison argument, the second is the result upon a true comparison, and the
third is the result upon a false comparison, basically a shortened way of writing an
if-else statement.*

#### Shell Variables

Shell variables are allowed as operands; parameter expansion is performed before the
expression is evaluated. A shell variable that is null, unset or non-numeric evaluates
to __0__ when referenced. The value of a variable is evaluated as an arithmetic
expression when it is referenced, or when a variable which has been given the integer
attribute using __*declare -i*__ is assigned a value. All null values evaluate to
__0__. A shell variable need not have its integer attribute turned on to be used in an
expression. Within an expression, shell variables may also be referenced simply by name
without using __'$'__ or __'${...}'__.

#### Specifying Numeric Base

Constants with a leading __0__ are interpreted as octal numbers. A leading __0x__ or
__0X__ denotes hexadecimal. Otherwise, numbers take the form __*'base#*__ __n'__, where
the optional __*base#*__ is a decimal number between __2__ and __64__ representing the
arithmetic base, and __n__ is a number in that base. If __*base#*__ is omitted, then
__10#__ is assumed. When specifying __n__, the digits greater than __9__ are
represented by the lowercase letters __*{a...z}*__, the uppercase letters
__*{A...Z}*__, __@__, and __'_'__, in that order. If base is less than or equal to
__36__, lowercase and uppercase letters may be used interchangeably to represent
numbers between __10__ and __35__.

## Process Substitution

Process substitution allows a process's input or output to be referred to using a
filename. Process substitution is supported on systems that support named pipes (FIFOs)
or the __*/dev/fd*__ method of naming open files. It takes the form of __*<(list)*__ or
__*>(list)*__. The process __*list*__ is run asynchronously, and its input or output
appears as a filename. This filename is passed as an argument to the current command as
the result of the expansion.

* __*>(list)*__ : writing to the file will provide input for __*list*__
* __*<(list)*__ : the file passed as an argument should be read to obtain the output of
__*list*__

When available, _process substitution_ is performed simultaneously with;

* parameter and variable expansion,
* command substitution,
* arithmetic expansion.

Useful to avoid invoking execution subshell, consider;

```bash
    echo "random input" | while read i
    echo $i
    do
      global="3D"
    done
    echo "global="${global}
```
result;

    user$ random input
    user$ global=

Variable not set because a subshell was invoked, however;

```bash
    while read i
    do
      echo $i
      global="3D"
    done < <( echo "random input" )
    #    ^ ^  First < is redirection, second is process substitution.
```
result;

    user$ random input
    user$ global=3D

Variable set because a subshell was not invoked.

See the
__*[Advanced Bash Scripting Guide](http://tldp.org/LDP/abs/html/process-sub.html)*__
for a more complete explanation.

## Word Splitting

### Invocation

When not invoked within double quotes, the results of;

* parameter expansion,
* command substitution,
* arithmetic expansion

are scanned by bash for word splitting.

### Operation

Bash treats each character of __*IFS*__ as a delimiter, and splits the results of the
other expansions into words using these characters as field terminators. If the value
of __*IFS*__ is null, no word splitting occurs.

Explicit null arguments (__""__ or __''__) are retained and passed to commands as empty
strings. Unquoted implicit null arguments, resulting from the expansion of parameters
that have no values, are removed. If a parameter with no value is expanded within
double quotes, a null argument results and is retained and passed to a command as an
empty string. When a quoted null argument appears as part of a word whose expansion is
non-null, the null argument is removed. That is, the word __*-d''*__ becomes __*-d*__
after word splitting and null argument removal.

__NB__ If no expansion occurs, no splitting is performed.

#### IFS Unset or Default Values

Sequences of __*[space]*__, __*[tab]*__, and __*[newline]*__ (the default) at the
beginning and end of the results of the previous expansions are ignored, and any
sequence of __*IFS*__ characters not at the beginning or end serves to delimit words.

#### IFS Not Default Values

Sequences of the whitespace characters __*[space]*__, __*[tab]*__, and __*[newline]*__
are ignored at the beginning and end of the word, as long as the __*IFS*__ whitespace
character is in the value of __*IFS*__. Any character in __*IFS*__ that is not
__*IFS*__ whitespace, along with any adjacent __*IFS*__ whitespace characters, delimits
a field. A sequence of __*IFS*__ whitespace characters is also treated as a delimiter.

## Pathname Expansion

After word splitting, unless the __*-f*__ option has been set, bash scans each word for the
characters __*'\*'*__, __*'?'*__, and __*'['*__. If one of these characters appears, then the word is
regarded as a pattern, and replaced with an alphabetically sorted list of filenames
matching the pattern (see [Pattern Matching](#pattern-matching)).

### Shell Options

* __*nullglob*__ not set : if no matching filenames are found the word is left
unchanged.
* __*nullglob*__ set : if no matching filenames are found, the word is removed.
* __*failglob*__ set : if no matches are found, an error message is printed and
the command is not executed.
* __*nocaseglob*__ set, match is performed without regard to the case of alphabetic
characters.
* __*dotglob*__ set : the character __'.'__ at the start of a name or immediately
following a slash is ignored when matching.

When matching a pathname, the slash character must always be matched explicitly. In
other cases, the __*.*__ character is not treated specially.

### GLOBIGNORE

This shell variable may be used to restrict the set of filenames matching a pattern.

* if set, each matching filename that also matches one of the patterns it contains is
removed from the list of matches.
* if the __*nocaseglob*__ option is set, the matching of patterns in __*GLOBIGNORE*__
is performed without regard to case. The filenames __*'.'*__ and __*'..'*__ are always
ignored when it is set and not null. However, setting it to a non-null value has the
effect of enabling the __*dotglob*__ shell option, so all other filenames beginning
with a __*'.'*__ will match.
* To get the old behavior of ignoring filenames beginning with a __*'.'*__, make
__*'.\*'*__ one of its patterns.
* The __*dotglob*__ option is disabled when it is unset. The pattern matching honors
the setting of the __*extglob*__ shell option.

### Pattern Matching
(see [Conditions](./Conditions.md#pattern-matching))

## Quote Removal
After the preceding expansions, all unquoted occurrences of the characters __*'\\'*__,
__*'*__, and __*"*__ that did not result from one of the above expansions are removed.
