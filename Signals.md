# Signals

## Signal Dispositions
Each signal has a current disposition, which determines how the process behaves when it
is delivered the signal. The signal disposition is a per-process attribute: in a
multithreaded application, the disposition of a particular signal is  the  same for all
threads.

## Standard Signals
|    Signal       |  Value   |  Action |  Comment                                                                |
| ------------    | -------- | ------- | ----------------------------------------------------------------------- |
| __SIGHUP__      |   01     | Term    | Hangup detected on controlling terminal or death of controlling process |
| __SIGINT__      |   02     | Term    | Interrupt from keyboard                                                 |
| __SIGQUIT__     |   03     | Core    | Quit from keyboard                                                      |
| __SIGILL__      |   04     | Core    | Illegal Instruction                                                     |
| __SIGTRAP__     |   05 (1) | Core    | Trace/breakpoint trap                                                   |
| __SIGABRT__     |   06     | Core    | Abort signal from abort(3)                                              |
| __SIGBUS__      |   07 (1) | Core    | Bus error (bad memory access)                                           |
| __SIGFPE__      |   08     | Core    | Floating-point exception                                                |
| __SIGKILL__     |   09 (3) | Term    | Kill signal                                                             |
| __SIGUSR1__     |   10     | Term    | User-defined signal 1                                                   |
| __SIGSEGV__     |   11     | Core    | Invalid memory reference                                                |
| __SIGUSR2__     |   12     | Term    | User-defined signal 2                                                   |
| __SIGPIPE__     |   13     | Term    | Broken pipe: write to pipe with no readers; see pipe(7)                 |
| __SIGALRM__     |   14     | Term    | Timer signal from alarm(2)                                              |
| __SIGTERM__     |   15     | Term    | Termination signal                                                      |
| __SIGSTKFLT__   |   16 (2) | Term    | Stack fault on coprocessor (unused)                                     |
| __SIGCHLD__     |   17     | Ign     | Child stopped or terminated                                             |
| __SIGCONT__     |   18     | Cont    | Continue if stopped                                                     |
| __SIGSTOP__     |   19 (3) | Stop    | Stop process                                                            |
| __SIGTSTP__     |   20     | Stop    | Stop typed at terminal                                                  |
| __SIGTTIN__     |   21     | Stop    | Terminal input for background process                                   |
| __SIGTTOU__     |   22     | Stop    | Terminal output for background process                                  |
| __SIGURG__      |   23 (1) | Ign     | Urgent condition on socket (4.2BSD)                                     |
| __SIGXCPU__     |   24 (1) | Core    | CPU time limit exceeded (4.2BSD); see setrlimit(2)                      |
| __SIGXFSZ__     |   25 (1) | Core    | File size limit exceeded (4.2BSD); see setrlimit(2)                     |
| __SIGVTALRM__   |   26 (1) | Term    | Virtual alarm clock (4.2BSD)                                            |
| __SIGPROF__     |   27 (1) | Term    | Profiling timer expired                                                 |
| __SIGWINCH__    |   28 (2) | Ign     | Window resize signal (4.3BSD, Sun)                                      |
| __SIGIO__       |   29 (2) | Term    | I/O now possible (4.2BSD)                                               |
| __SIGPWR__      |   30 (2) | Term    | Power failure (System V)                                                |
| __SIGSYS__      |   31 (1) | Core    | Bad system call (SVr4);. see also seccomp(2)                            |
| __SIGRTMIN__    |   34 (4) | Ign     | Application defined first real-time signal                              |
| __SIGRTMIN+1__  |   35 (4) | Ign     | Application defined second real-time signal                             |
| __SIGRTMIN+2__  |   36 (4) | Ign     | Application defined third real-time signal                              |
| __:__           |    :     |  :      |                        :                                                |
| __:__           |    :     |  :      |                        :                                                |
| __SIGRTMIN+15__ |   49 (4) | Ign     | Application defined sixteenth real-time signal                          |
| __SIGRTMAX-14__ |   50 (4) | Ign     | Application defined seventeenth real-time signal                        |
| __SIGRTMAX-13__ |   51 (4) | Ign     | Application defined eighteenth real-time signal                         |
| __:__           |    :     |  :      |                        :                                                |
| __:__           |    :     |  :      |                        :                                                |
| __SIGRTMAX-2__  |   62 (4) | Ign     | Application defined antepenultimate real-time signal                    |
| __SIGRTMAX-1__  |   63 (4) | Ign     | Application defined penultimate real-time signal                        |
| __SIGRTMAX__    |   64 (4) | Ign     | Application defined last real-time signal                               |
     Unless otherwise annotated all signals are as in the original POSIX.1-1990 standard:
     1. Signals not in the POSIX.1-1990 standard but described in SUSv2 and POSIX.1-2001.
     2. Other 'undefined' signals.
     3. Signals that cannot be caught, blocked, or ignored.
     4. On POSIX-compliant platforms, SIGRTMIN through SIGRTMAX are signals sent to
        computer programs for user-defined purposes. The symbolic constants for
        SIGRTMIN and SIGRTMAX are defined in the header file signal.h. Symbolic signal
        names are used because signal numbers can vary across platforms.

The entries in the "Action" column specify the default disposition for each signal:

* __Cont__ - continue the process if it is currently stopped.
* __Core__ - terminate the process and dump core (see core(5)).
* __Ign__  - ignore the signal.
* __Stop__ - stop the process.
* __Term__ - terminate the process.

__SIGEMT__ is not specified in POSIX.1-2001, but nevertheless appears on most other UNIX
systems, where its default action is typically to terminate the process with a core
dump.

__SIGPWR__ (which is not specified in POSIX.1-2001) is typically ignored by default on
those other UNIX systems where it appears.

__SIGIO__ (which is not specified in POSIX.1-2001) is ignored by default on several other
UNIX systems.

Where defined, __SIGUNUSED__ is synonymous with __SIGSYS__ on most architectures.

## The Bash Shell

When bash is interactive, in the absence of any traps, it ignores __SIGTERM__ (so that
__kill 0__ does not kill an interactive shell), and __SIGINT__  is  caught  and
handled (so that the __*wait*__ builtin is interruptible).  In all cases, bash ignores
__SIGQUIT__. If job control is in effect, bash ignores __SIGTTIN__, __SIGTTOU__, and
__SIGTSTP__.

Non-builtin commands run by bash have signal handlers set to the values inherited by
the shell from its parent.  When  job  control  is not  in  effect,  asynchronous
commands ignore __SIGINT__ and __SIGQUIT__ in addition to these inherited handlers.
Commands run as a result of command substitution ignore the keyboard-generated job
control signals __SIGTTIN__, __SIGTTOU__, and __SIGTSTP__.

The shell exits by default upon receipt of a __SIGHUP__.  Before exiting, an
interactive shell resends the __SIGHUP__ to all jobs,  running  or stopped.  Stopped
jobs are sent __SIGCONT__ to ensure that they receive the __SIGHUP__.  To prevent the
shell from sending the signal to a par‐ ticular job, it should be removed from the jobs
table with the __*disown*__ builtin or  marked  to  not receive __SIGHUP__ using disown -h.

If the huponexit shell option has been set with shopt, bash sends a __SIGHUP__ to all
jobs when an interactive login shell exits.

If  bash  is waiting for a command to complete and receives a signal for which a trap
has been set, the trap will not be executed until the command completes.  When bash is
waiting for an asynchronous command via the __*wait*__ builtin, the reception of a signal for
which  a trap  has  been set will cause the __*wait*__ builtin to return immediately with an
exit status greater than 128, immediately after which the trap is executed.

### Sending Signals Using The Shell

The following signals can be sent using the Bash shell:

Ctrl+C

     The interrupt signal, sends SIGINT to the job running in the foreground.

Ctrl+Y

     The delayed suspend character. Causes a running process to be stopped when it
     attempts to read input from the terminal. Control is returned to the shell, the
     user can foreground, background or kill the process. Delayed suspend is only
     available on operating systems supporting this feature.

Ctrl+Z

     The suspend signal, sends a SIGTSTP to a running program, thus stopping it and
     returning control to the shell.

Note - Terminal settings

    Depending on the stty settings, suspend and resume of output is usually disabled
    with "modern" terminal emulations. The standard xterm supports Ctrl+S and Ctrl+Q by
    default.

### Usage Of Signals With __*kill*__

Most modern shells, Bash included, have a built-in __*kill*__ function. In Bash, both
signal names and numbers are accepted as options, and arguments may be job or process
IDs. An exit status can be reported using the -l option: zero when at least one signal
was successfully sent, non-zero if an error occurred.

Using the __*kill*__ command from /usr/bin, your system might enable extra options,
such as the ability to __*kill*__ processes from other than your own user ID and
specifying processes by name, like with __*pgrep*__ and __*pkill*__.

Both __*kill*__ commands send the __SIGTERM__ signal if none is given.

When killing a process or series of processes, it is common sense to start trying
with the least dangerous signal, __SIGTERM__. That way, programs that care about an
orderly shutdown get the chance to follow the procedures that they have been designed
to execute when getting the __SIGTERM__ signal, such as cleaning up and closing open
files. If you send a __SIGKILL__ to a process, you remove any chance for the
process to do a tidy cleanup and shutdown, which might have unfortunate consequences.

But if a clean termination does not work, the __SIGINT__ or __SIGKILL__ signals
might be the only way. For instance, when a process does not die using Ctrl+C, it is
best to use the __*kill*__ -9 on that process ID.

When a process starts up several instances, __*killall*__ might be easier. It takes
the same option as the __*kill*__ command, but applies on all instances of a given
process. Test this command before using it in a production environment, since it might
not work as expected on some of the commercial Unices.
